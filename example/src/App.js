import React from 'react';
import { Button } from 'antd';
import Detail from './Detail';
import Create from './Create';
import Edit from './Edit';
import List from './List';
import Search from './Search';

export default class extends React.PureComponent {
    state = {
        page: "",
    };

    render() {
        switch (this.state.page) {
            case "list":
                return <List />;
            case "create":
                return <Create />;
            case "edit":
                return <Edit />;
            case "detail":
                return <Detail />;
            case "search":
                return <Search />;
            default:
                return this._renderMain();
        }
    }

    _renderMain = () => {
        return (
            <div>
                <Button type="link" onClick={this._onClick.bind(this, "list")}>
                    查看列表页样例
                </Button>
                <Button type="link" onClick={this._onClick.bind(this, "detail")}>
                    查看详情页样例
                </Button>
                <Button type="link" onClick={this._onClick.bind(this, "create")}>
                    查看新建页样例
                </Button>
                <Button type="link" onClick={this._onClick.bind(this, "edit")}>
                    查看编辑页样例
                </Button>
                <Button type="link" onClick={this._onClick.bind(this, "search")}>
                    查看搜索页样例
                </Button>
            </div>
        );
    };

    _onClick = (page) => {
        this.setState({page});
    };
}
