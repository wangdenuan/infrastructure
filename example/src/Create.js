import * as React from 'react';
import { Config, Page } from '@xiaochengtech/infrastructure';
import * as Fields from './Field';

export default class extends React.PureComponent {
    render() {
        return (
            <Page.Create
                formProps={{
                    labelCol: {
                        xs: { span: 3 },
                        sm: { span: 3 },
                    },
                    wrapperCol: {
                        xs: { span: 10 },
                        sm: { span: 10 },
                    },
                }}
                fields={PageConfig}
                commitData={(value) => Promise.resolve()}
                onChangeField={(name, value) => console.log(name + " " + JSON.stringify(value))}
            />
        );
    }
}

const PageConfig = [
    /* Config.Digit.NewCreate(Fields.ID, {
        required: true,
    }),
    Config.Text.NewCreate(Fields.Name, {
        required: true,
    }),
    Config.Digit.NewCreate(Fields.Count, {
        required: true,
    }),
    Config.Switch.NewCreate(Fields.Gender, {
        required: true,
    }),
    Config.DateTime.NewCreate(Fields.OneDate),
    Config.DateTime.NewCreate(Fields.OneDateTime),
    Config.Select.NewCreate(Fields.SingleSelect),
    Config.Select.NewCreate(Fields.MultiSelect),
    Config.TreeSelect.NewCreate(Fields.TreeSelect),
    Config.CascadeSelect.NewCreate(Fields.Location),
    Config.BitSelect.NewCreate(Fields.Configuration), */
    //Config.CharSelect.NewCreate(Fields.Authrise),
    Config.TextArea.NewCreate(Fields.Description),
];
