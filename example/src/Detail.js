import * as React from 'react';
import { Config, Page } from '@xiaochengtech/infrastructure';
import * as Fields from './Field';

export default class extends React.PureComponent {
    render() {
        return (
            <Page.Detail
                fields={PageConfig}
                data={DetailData}
                descriptionProps={{
                    column: 1,
                }}
            />
        );
    }
}

export const DetailData = {
    id: 1,
    name: "测试名称123",
    count: 15,
    gender: false,
    one_date: "2018-03-12",
    one_date_time: Math.floor(Date.now() / 1000),
    single_select: 1,
    multi_select: ["red", "blue", "hello", "white"],
    tree_select: [1, 3, 4],
    province: "101",
    city: "300",
    district: undefined,
    configuration: 6,
    authrise: "Vc",
    description: "公共停车场是根据城市规划建造以及公共建筑配套专供社会车辆停放的（露天或室内）收费营业性停车场，称公共停车场。而公共停车场又分为路内、路外两种：一、路内临时性公共停车场路内临时停车场是指经批准，在规定时限，占用城市部分道路，允许停放大量车辆，以满足特大型活动（游行、集会等）的临时场地。"
};

const PageConfig = [
    Config.Digit.NewDetail(Fields.ID),
    Config.Text.NewDetail(Fields.Name),
    Config.Digit.NewDetail(Fields.Count, { step: 0.1 }),
    Config.Switch.NewDetail(Fields.Gender),
    Config.DateTime.NewDetail(Fields.OneDate),
    Config.DateTime.NewDetail(Fields.OneDateTime),
    Config.Select.NewDetail(Fields.SingleSelect),
    Config.Select.NewDetail(Fields.MultiSelect),
    Config.TreeSelect.NewDetail(Fields.TreeSelect),
    Config.CascadeSelect.NewDetail(Fields.Location),
    Config.BitSelect.NewDetail(Fields.Configuration),
    Config.CharSelect.NewDetail(Fields.Authrise),
    Config.TextArea.NewDetail(Fields.Description),
];
