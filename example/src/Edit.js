import * as React from 'react';
import { Config, Page } from '@xiaochengtech/infrastructure';
import * as Fields from './Field';
import { DetailData } from './Detail';

export default class extends React.PureComponent {
    render() {
        return (
            <Page.Edit
                fields={PageConfig}
                data={DetailData}
                commitData={(value) => Promise.resolve()}
                onChangeField={(name, value) => console.log(name + " " + JSON.stringify(value))}
            />
        );
    }
}

const PageConfig = [
    Config.Digit.NewEdit(Fields.ID, {
        required: true,
        readonly: true,
    }),
    Config.Text.NewEdit(Fields.Name, {
        required: true,
    }),
    Config.Digit.NewEdit(Fields.Count, {
        required: true,
    }),
    Config.Switch.NewEdit(Fields.Gender, {
        required: true,
    }),
    Config.DateTime.NewEdit(Fields.OneDate),
    Config.DateTime.NewEdit(Fields.OneDateTime),
    Config.Select.NewEdit(Fields.SingleSelect),
    Config.Select.NewEdit(Fields.MultiSelect),
    Config.TreeSelect.NewEdit(Fields.TreeSelect),
    Config.BitSelect.NewEdit(Fields.Configuration),
    Config.CharSelect.NewEdit(Fields.Authrise),
    Config.TextArea.NewEdit(Fields.Description),
];
