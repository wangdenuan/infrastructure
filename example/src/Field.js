import { Config } from '@xiaochengtech/infrastructure';

export const ID = Config.Digit.NewBase("id", "序号");
export const Name = Config.Text.NewBase("name", "名称");
export const Count = Config.Digit.NewBase("count", "数量", {
    unitLabel: "个",
});
export const Gender = Config.Switch.NewBase("gender", "性别", {
    trueLabel: "男",
    falseLabel: "女",
});
export const OneDate = Config.DateTime.NewBase("one_date", "指定日期", {
    hasDate: true,
    hasTime: false,
    useTimestamp: false,
    dateFormat: "YYYY-MM-DD",
});
export const OneDateTime = Config.DateTime.NewBase("one_date_time", "指定日期时间", {
    hasDate: true,
    hasTime: true,
    dateFormat: "YYYY/MM/DD-HH:mm:ss",
    useTimestamp: true,
    useSecond: true,
});
export const SingleSelect = Config.Select.NewBase("single_select", "单选项", {
    multiple: false,
    valueType: "number",
    dataSource: [
        { value: 0, label: "测试环境" },
        { value: 1, label: "预发环境" },
        { value: 2, label: "正式环境" },
    ],
});
export const MultiSelect = Config.Select.NewBase("multi_select", "多选项", {
    multiple: true,
    dataSource: [
        { value: "red", label: "红色" },
        { value: "blue", label: "蓝色" },
        { value: "green", label: "绿色" },
        { value: "black", label: "黑色" },
        { value: "white", label: "白色" },
    ],
    unknownLabel: "未知色",
});
export const TreeSelect = Config.TreeSelect.NewBase("tree_select", "树状项", {
    multiple: true,
    getDataSource: () => {
        const tree = [
            {
                value: 0, title: "1-1", key: "1-1", children: [
                    {
                        value: 1, title: "2-1", key: "2-1", children: [
                            { value: 3, title: "3-1", key: "3-1" },
                            { value: 4, title: "3-2", key: "3-2" },
                        ]
                    },
                    {
                        value: 2, title: "2-2", key: "2-2", children: [
                            { value: 5, title: "4-1", key: "4-1" },
                        ]
                    },
                ]
            },
        ];
        return Promise.resolve(tree);
    },
    unknownLabel: "未知叶节点",
});
export const Location = Config.CascadeSelect.NewBase("location", "定位", {
    getLevelOptions: (values) => {
        if (values.length === 0) {
            return Promise.resolve([
                { value: "100", label: "省1", isLeaf: false },
                { value: "101", label: "省2", isLeaf: false },
            ]);
        } else if (values.length === 1) {
            if (values[0].value === "100") {
                return Promise.resolve([
                    { value: "200", label: "市1-1", isLeaf: false },
                    { value: "201", label: "市1-2", isLeaf: true },
                ]);
            } else if (values[0].value === "101") {
                return Promise.resolve([
                    { value: "300", label: "市2-1", isLeaf: false },
                    { value: "301", label: "市2-2", isLeaf: true },
                ]);
            }
        } else {
            return Promise.resolve([
                { value: "401", label: "区1", isLeaf: true },
                { value: "402", label: "区2", isLeaf: true },
                { value: "403", label: "区3", isLeaf: true },
            ]);
        }
    },
    getLabel: (value) => {
        const itemMap = {
            "100": "省1",
            "101": "省2",
            "200": "市1-1",
            "201": "市1-2",
            "300": "市2-1",
            "301": "市2-2",
            "401": "区1",
            "402": "区2",
            "403": "区3",
        };
        return Promise.resolve(itemMap[value] || "未知项");
    },
    fieldNames: ["province", "city", "district"],
});
export const Configuration = Config.BitSelect.NewBase("configuration", "详细配置", {
    base: 4,
    dataSource: [
        {
            index: 0,
            defaultValue: 0,
            fieldLabel: "方向",
            values: [0, 1, 2],
            labels: ["都不是", "东西", "南北"],
        },
        {
            index: 1,
            defaultValue: 3,
            fieldLabel: "操作",
            values: [0, 1, 3],
            labels: ["无权限", "仅当前项", "所有项"],
        },
    ],
});

export const Authrise = Config.CharSelect.NewBase("authrise", "权限配置", {
    dataSource: [
        {
            defaultValue: "",
            fieldLabel: "查看",
            fieldValue: "v",
            labels: ["无权限", "当前组织", "当前组织及子组织"],
        },
        {
            defaultValue: "",
            fieldLabel: "新增",
            fieldValue: "c",
            labels: ["无权限", "当前组织", "当前组织及子组织"],
        },
    ],
});
export const Description = Config.TextArea.NewBase("description", "描述", {
    placeholder: "aaaaaaaaa",
    valueWidth: 200,
    valueHeight: 60,
    minRows: 2,
    maxRows: 6
});
