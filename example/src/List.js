import * as React from 'react';
import { Config, Page } from '@xiaochengtech/infrastructure';
import * as Fields from './Field';

export default class extends React.PureComponent {
    render() {
        return (
            <Page.List
                fields={PageConfig}
                pageSize={2}
                mainKey={Fields.ID.name}
                fetchData={(pageNumber, pageSize) => Promise.resolve({count: ListData.length, data: ListData.slice((pageNumber - 1) * pageSize, pageNumber * pageSize)})}
                onRow={(item) => ({
                    onClick: () => {
                        alert(JSON.stringify(item));
                    },
                })}
            />
        );
    }
}

const ListData = [
    {
        id: "1",
        name: "测试1",
        count: 15,
        gender: false,
    },
    {
        id: "2",
        name: "测试2",
        count: 30,
        gender: true,
    },
    {
        id: "3",
        name: "测试3",
        count: 15,
        gender: false,
    },
    {
        id: "4",
        name: "测试4",
        count: 30,
        gender: true,
    },
];

const PageConfig = [
    Config.Digit.NewList(Fields.ID),
    Config.Text.NewList(Fields.Name),
    Config.Digit.NewList(Fields.Count),
    Config.Switch.NewList(Fields.Gender),
];
