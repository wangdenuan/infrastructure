import * as React from 'react';
import { Config, Page, Util } from '@xiaochengtech/infrastructure';
import * as Fields from './Field';

export default class extends React.PureComponent {
    render() {
        return (
            <Page.Search
                fields={PageConfig}
                data={{}}
                columns={3}
                commitData={this._processData}
                onChangeField={this._onChangeField}
            />
        );
    }

    _onChangeField = (name, value) => {
        console.log("change search field", name, value);
    };

    _processData = async (value) => {
        console.log(value);
        const newValue = Util.Condition.process(value);
        console.log(newValue);
        return;
    };
}

const PageConfig = [
    Config.Text.NewSearch(Fields.Name),
    Config.Select.NewSearch(Fields.SingleSelect),
    Config.Select.NewSearch(Fields.MultiSelect),
    Config.DateTime.NewSearch(Fields.OneDate),
    Config.DateTime.NewSearch(Fields.OneDateTime),
];
