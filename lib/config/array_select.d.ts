/**
 * 按number选择类型。
 */
import * as Base from './base';
export declare const Type = "array_select";
export interface OptionItem {
    /**
     * 默认值。
     */
    defaultValue: number;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 字段的前缀值，不为空时与数字值用"-"连接形成返回结果。
     */
    preValue: string;
    /**
     * 字段的数字值。
     */
    values: number[];
    /**
     * 显示的标签，与数字值一一对应
     */
    labels: string[];
}
export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 数据源字段标签的显示长度。
     */
    colSpan: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
