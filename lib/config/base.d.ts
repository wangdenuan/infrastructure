import { ColumnProps } from 'antd/lib/table';
/**
 * 字段基础配置。
 */
export interface Options {
    [key: string]: any;
}
export declare const DefaultOptions: Options;
/**
 * 字段，T是配置信息。
 */
export interface Config<T extends Options = Options> {
    /**
     * 字段名。
     */
    name: string;
    /**
     * 页面上展示的文字。
     */
    label: string;
    /**
     * 字段类型。
     */
    type: string;
    /**
     * 配置信息。
     */
    options: T;
}
/**
 * 基础字段生成器。
 */
export declare function BaseGenerator<T>(type: string, defaultOptions: T): (name: string, label: string, options?: Partial<T & Options>) => Config<T & Options>;
/**
 * 适当情景下的字段生成器。
 */
export declare function Generator<T, P>(defaultOptions: P): (base: Config<T & Options>, options?: Partial<T & P & Options>) => Config<T & P & Options>;
/**
 * 列表字段。
 */
export interface ListOptions {
    /**
     * 是否展示。
     */
    showInList: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 自定义表格列属性。
     */
    tableProps?: ColumnProps<any>;
}
export declare const DefaultListOptions: ListOptions;
export declare type ListConfig = Config<ListOptions & Options>;
/**
 * 新建字段。
 */
export interface CreateOptions {
    /**
     * 是否展示。
     */
    showInCreate: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 是否是必填项。
     */
    required: boolean;
    /**
     * 默认值。
     */
    defaultValue: any;
}
export declare const DefaultCreateOptions: CreateOptions;
export declare type CreateConfig = Config<CreateOptions & Options>;
/**
 * 编辑字段。
 */
export interface EditOptions {
    /**
     * 是否展示。
     */
    showInEdit: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 是否是必填项。
     */
    required: boolean;
    /**
     * 是否是只读字段。
     */
    readonly: boolean;
}
export declare const DefaultEditOptions: EditOptions;
export declare type EditConfig = Config<EditOptions & Options>;
/**
 * 详情字段。
 */
export interface DetailOptions {
    /**
     * 是否展示。
     */
    showInDetail: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * Span比例。
     */
    span?: number;
}
export declare const DefaultDetailOptions: DetailOptions;
export declare type DetailConfig = Config<DetailOptions & Options>;
/**
 * 搜索字段。
 */
export interface SearchOptions {
    /**
     * 是否展示。
     */
    showInSearch: boolean;
    /**
     * 栅格布局的列宽。
     */
    colSpan: number;
    /**
     * 标签。
     */
    label: string;
    /**
     * 默认值。
     */
    defaultValue: any;
}
export declare const DefaultSearchOptions: SearchOptions;
export declare type SearchConfig = Config<SearchOptions & Options>;
