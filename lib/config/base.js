"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultOptions = {};
/**
 * 基础字段生成器。
 */
function BaseGenerator(type, defaultOptions) {
    return function (name, label, options = {}) {
        const opt = Object.assign(Object.assign(Object.assign({}, exports.DefaultOptions), defaultOptions), options);
        return { name, label, type, options: opt };
    };
}
exports.BaseGenerator = BaseGenerator;
/**
 * 适当情景下的字段生成器。
 */
function Generator(defaultOptions) {
    return function (base, options = {}) {
        const opt = Object.assign(Object.assign(Object.assign({}, defaultOptions), base.options), options);
        return Object.assign(Object.assign({}, base), { options: opt });
    };
}
exports.Generator = Generator;
exports.DefaultListOptions = {
    showInList: true,
    label: "",
    tableProps: undefined,
};
exports.DefaultCreateOptions = {
    showInCreate: true,
    label: "",
    required: false,
    defaultValue: undefined,
};
exports.DefaultEditOptions = {
    showInEdit: true,
    label: "",
    required: false,
    readonly: false,
};
exports.DefaultDetailOptions = {
    showInDetail: true,
    label: "",
};
exports.DefaultSearchOptions = {
    showInSearch: true,
    colSpan: 6,
    label: "",
    defaultValue: undefined,
};
