/**
 * 按比特位选择类型。
 */
import * as Base from './base';
export declare const Type = "bit_select";
export interface OptionItem {
    /**
     * 第几个位置(从低位计数，从0开始)。
     */
    index: number;
    /**
     * 默认值。
     */
    defaultValue: number;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 取值的列表。
     */
    values: number[];
    /**
     * 显示的标签，与values一一对应。
     */
    labels: string[];
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
}
export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 基数，为2的幂数。
     */
    base: number;
    /**
     * 数据源字段标签的显示长度(仅用于编辑模式)。
     */
    colSpan: number;
    /**
     * 是否分散数据源显示(仅用于显示模式)。
     */
    row?: boolean;
    /**
     * 数据源的显示索引(仅用于显示模式)。
     */
    itemIndex?: number;
    /**
     * 值宽度(仅用于显示模式)。
     */
    valueWidth?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
