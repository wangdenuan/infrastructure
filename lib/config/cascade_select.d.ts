/**
 * 级联选择类型。
 */
import { CascaderOptionType } from 'antd/lib/cascader';
import * as Base from './base';
export declare const Type = "cascade_select";
export interface Options {
    /**
     * 静态数据源。
     */
    dataSource?: CascaderOptionType[];
    /**
     * 动态数据源。
     * @param values 已选中的项。
     */
    getLevelOptions?: (values: CascaderOptionType[]) => Promise<CascaderOptionType[]>;
    /**
     * 获取值对应的展示标签。
     */
    getLabel: (value: any) => Promise<string>;
    /**
     * 自动处理字段。
     */
    fieldNames: string[];
    /**
     * 占位文本。
     */
    placeholder: string | undefined;
    /**
     * 展示时的分隔符。
     */
    separator: string;
    /**
     * 值宽度。
     */
    valueWidth?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
