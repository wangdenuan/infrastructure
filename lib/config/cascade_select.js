"use strict";
/**
 * 级联选择类型。
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base = __importStar(require("./base"));
exports.Type = "cascade_select";
exports.DefaultOptions = {
    dataSource: undefined,
    getLevelOptions: undefined,
    getLabel: (value) => Promise.resolve("" + value),
    fieldNames: [],
    placeholder: undefined,
    separator: "-",
};
exports.NewBase = Base.BaseGenerator(exports.Type, exports.DefaultOptions);
exports.NewList = Base.Generator(Base.DefaultListOptions);
exports.NewCreate = Base.Generator(Base.DefaultCreateOptions);
exports.NewEdit = Base.Generator(Base.DefaultEditOptions);
exports.NewDetail = Base.Generator(Base.DefaultDetailOptions);
exports.NewSearch = Base.Generator(Base.DefaultSearchOptions);
