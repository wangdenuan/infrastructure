/**
 * 按char选择类型。
 */
import * as Base from './base';
export declare const Type = "char_select";
export interface OptionItem {
    /**
     * 默认值。
     */
    defaultValue: string;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 字段的基础值，自动分解为空串，小写和大写。
     */
    fieldValue: string;
    /**
     * 显示的标签，与值的空串，小写和大写一一对应，数组长度只能为3。
     */
    labels: string[];
}
export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 数据源字段标签的显示长度。
     */
    colSpan: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
