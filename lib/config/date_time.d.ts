/**
 * 日期/时间类型。
 */
import { TimePickerProps } from 'antd/lib/time-picker/';
import * as Base from './base';
export declare const Type = "date_time";
export interface Options {
    /**
     * 是否包含日期信息，不能与hasTime同时为false。
     */
    hasDate: boolean;
    /**
     * 是否包含时间信息，不能与hasDate同时为false。
     */
    hasTime: boolean | TimePickerProps;
    /**
     * 是否使用时间戳表示数据。
     */
    useTimestamp: boolean;
    /**
     * 是否使用秒来表示数据，仅当useTimestamp=true时有意义。
     */
    useSecond: boolean;
    /**
     * 日期时间格式化的格式。
     */
    dateFormat: string | undefined;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 开始时间默认值(仅用于搜索)。
     */
    startTimeValue: number;
    /**
     * 结束时间默认值(仅用于搜索)。
     */
    endTimeValue: number;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
