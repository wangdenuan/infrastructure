/**
 * 数字类型。
 */
import * as Base from './base';
export declare const Type = "digit";
export interface Options {
    /**
     * 最大值。
     */
    max?: number;
    /**
     * 最小值。
     */
    min?: number;
    /**
     * 整数小数步长，如果为0.1，则表示小数只能有1位。
     */
    step: number;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}
export declare const DefaultOptions: Options;
export declare function GetDotBit(value: number): number;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
