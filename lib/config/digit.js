"use strict";
/**
 * 数字类型。
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base = __importStar(require("./base"));
exports.Type = "digit";
exports.DefaultOptions = {
    step: 1,
};
// 根据步长获取保留的小数位
function GetDotBit(value) {
    // 获取小数点的位置
    const idx = String(value).indexOf(".");
    if (idx < 0) {
        return 0;
    }
    // 获取小数点后的个数
    return String(value).length - (idx + 1);
}
exports.GetDotBit = GetDotBit;
;
exports.NewBase = Base.BaseGenerator(exports.Type, exports.DefaultOptions);
exports.NewList = Base.Generator(Base.DefaultListOptions);
exports.NewCreate = Base.Generator(Base.DefaultCreateOptions);
exports.NewEdit = Base.Generator(Base.DefaultEditOptions);
exports.NewDetail = Base.Generator(Base.DefaultDetailOptions);
exports.NewSearch = Base.Generator(Base.DefaultSearchOptions);
