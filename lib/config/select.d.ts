/**
 * 单选或多选类型。
 */
import * as Base from './base';
export declare const Type = "select";
export interface OptionItem {
    /**
     * 数值。
     */
    value: string | number;
    /**
     * 标签。
     */
    label: string;
}
export interface Options {
    /**
     * 是否是多选。
     */
    multiple: boolean;
    /**
     * 数据类型，string|number。
     */
    valueType: string;
    /**
     * 多选数量限制，0无限制。
     */
    limitCount: number;
    /**
     * 静态数据源。
     */
    dataSource?: OptionItem[];
    /**
     * 动态源获取数据源。
     */
    getDataSource?: () => Promise<OptionItem[]>;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 是否启用过滤。
     */
    filtered?: boolean;
    /**
     * 未知选项标签。
     */
    unknownLabel: string;
    /**
     * 全部选项的索引，只用于搜索。
     */
    allValue: string | number;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
