/**
 * 布尔类型。
 */
import * as Base from './base';
export declare const Type = "switch";
export interface Options {
    /**
     * 使用radio还是switch控件。
     */
    useRadio: boolean;
    /**
     * 布尔值真值对应的标签。
     */
    trueLabel: string;
    /**
     * 布尔值假值对应的标签。
     */
    falseLabel: string;
    /**
     * 是否在呈现结果时增加圆点的显示(仅在detail中有效，为true时不显示单位)。
     */
    useDot: boolean;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
