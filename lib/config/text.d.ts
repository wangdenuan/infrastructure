/**
 * 文本类型。
 */
/// <reference types="react" />
import * as Base from './base';
export declare const Type = "text";
export interface Options {
    /**
     * 展示的行数，如果大于1，表示为多行文本。
     */
    numberOfLine: number;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * (文本字段) 最大长度，0不限制。
     */
    maxLength: number;
    /**
     * 正则匹配表达式。
     */
    regExp: string;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
    /**
     * 自定义渲染方法
     */
    render?: (text: string) => React.ReactNode;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
