/**
 * 文本类型。
 */
import * as Base from './base';
export declare const Type = "text_area";
export interface Options {
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * (文本字段) 最大长度，0不限制。
     */
    maxLength: number;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 值高度。
     */
    valueHeight?: number;
    /**
     * 最小行数。
     */
    minRows?: number;
    /**
     * 最大行数。
     */
    maxRows?: number;
}
export declare const DefaultOptions: Options;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
