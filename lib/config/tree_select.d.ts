/**
 * 树状选择类型。
 */
import { TreeNodeNormal } from 'antd/lib/tree-select/interface';
import * as Base from './base';
export declare const Type = "tree_select";
export interface Options {
    /**
     * 是否是多选。
     */
    multiple: boolean;
    /**
     * 数据类型，string|number。
     */
    valueType: string;
    /**
     * 多选数量限制，0无限制。
     */
    limitCount: number;
    /**
     * 数据源。
     */
    getDataSource: () => Promise<TreeNodeNormal[]>;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 未知选项标签。
     */
    unknownLabel: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean;
    /**
     * 值宽度。
     */
    valueWidth?: number;
}
export declare const DefaultOptions: Options;
export interface ModifyOptions {
    /**
     * 最大高度。
     */
    maxHeight: number;
    /**
     * 是否展开全部。
     */
    defaultExpandAll: boolean;
}
export declare const DefaultModifyOptions: ModifyOptions;
export declare const NewBase: (name: string, label: string, options?: Partial<Options & Base.Options>) => Base.Config<Options & Base.Options>;
export declare const NewList: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.ListOptions & Base.Options>) => Base.Config<Options & Base.ListOptions & Base.Options>;
export declare const NewCreate: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.CreateOptions & ModifyOptions & Base.Options>) => Base.Config<Options & Base.CreateOptions & ModifyOptions & Base.Options>;
export declare const NewEdit: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.EditOptions & ModifyOptions & Base.Options>) => Base.Config<Options & Base.EditOptions & ModifyOptions & Base.Options>;
export declare const NewDetail: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.DetailOptions & Base.Options>) => Base.Config<Options & Base.DetailOptions & Base.Options>;
export declare const NewSearch: (base: Base.Config<Options & Base.Options>, options?: Partial<Options & Base.SearchOptions & Base.Options>) => Base.Config<Options & Base.SearchOptions & Base.Options>;
