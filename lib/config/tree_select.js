"use strict";
/**
 * 树状选择类型。
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Base = __importStar(require("./base"));
exports.Type = "tree_select";
exports.DefaultOptions = {
    multiple: false,
    valueType: "string",
    limitCount: 0,
    getDataSource: () => Promise.resolve([]),
    placeholder: undefined,
    unknownLabel: "未知",
};
exports.DefaultModifyOptions = {
    maxHeight: 400,
    defaultExpandAll: true,
};
exports.NewBase = Base.BaseGenerator(exports.Type, exports.DefaultOptions);
exports.NewList = Base.Generator(Base.DefaultListOptions);
exports.NewCreate = Base.Generator(Object.assign(Object.assign({}, Base.DefaultCreateOptions), exports.DefaultModifyOptions));
exports.NewEdit = Base.Generator(Object.assign(Object.assign({}, Base.DefaultEditOptions), exports.DefaultModifyOptions));
exports.NewDetail = Base.Generator(Base.DefaultDetailOptions);
exports.NewSearch = Base.Generator(Base.DefaultSearchOptions);
