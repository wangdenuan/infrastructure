import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';
declare class BitSelect extends React.PureComponent<Storage.Option> {
    render(): JSX.Element | null;
    _getValueLabel: (item: Field.BitSelect.OptionItem) => string | null;
    _renderLine: (item: Field.BitSelect.OptionItem, key: number) => JSX.Element | null;
}
export default BitSelect;
