"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class BitSelect extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._getValueLabel = (item) => {
            const field = this.props.field;
            const data = this.props.data;
            let value = data[field.name], loc = item.index;
            while (loc > 0) {
                loc = loc - 1;
                value = Math.floor(value / field.options.base);
            }
            const finalValue = value % field.options.base;
            const vIndex = item.values.findIndex((i) => i === finalValue);
            if (vIndex < 0) {
                return null;
            }
            return item.labels[vIndex];
        };
        this._renderLine = (item, key) => {
            const valueLabel = this._getValueLabel(item);
            return valueLabel === null ? null : (React.createElement("div", { key: key, className: "xc-bit-select-view" },
                item.fieldLabel + ": " + valueLabel,
                Render.description(item.description, item.showTag)));
        };
    }
    render() {
        const field = this.props.field;
        const data = this.props.data;
        if (data[field.name] === undefined) {
            return null;
        }
        const row = field.options.row || false;
        if (row) {
            const idx = field.options.itemIndex || 0;
            const item = field.options.dataSource[idx];
            const valueLabel = this._getValueLabel(item);
            return (React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, valueLabel),
                Render.description(item.description, item.showTag)));
        }
        return (React.createElement("div", null, field.options.dataSource.map(this._renderLine)));
    }
}
Storage.set(Field.BitSelect.Type, (option) => React.createElement(BitSelect, Object.assign({}, option)));
exports.default = BitSelect;
