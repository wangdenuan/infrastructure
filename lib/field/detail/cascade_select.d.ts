import * as React from 'react';
import * as Storage from './storage';
interface State {
    label: string;
}
declare class CascadeSelect extends React.PureComponent<Storage.Option, State> {
    hasCustomField: boolean;
    constructor(props: Storage.Option);
    componentDidMount(): void;
    UNSAFE_componentWillReceiveProps(): void;
    render(): JSX.Element;
    _loadData: () => void;
}
export default CascadeSelect;
