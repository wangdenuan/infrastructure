"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
class CascadeSelect extends React.PureComponent {
    constructor(props) {
        super(props);
        this._loadData = () => {
            const field = this.props.field;
            const data = this.props.data;
            let value = data[field.name];
            if (this.hasCustomField) {
                value = field.options.fieldNames.map(i => data[i]);
            }
            if (value === undefined || !field.options.getLabel) {
                this.setState({ label: "" });
            }
            else {
                const arr = Array.isArray(value) ? value : [value];
                Promise.all(arr.filter(i => i !== undefined).map(i => field.options.getLabel(i)))
                    .then((result) => {
                    this.setState({ label: result.join(field.options.separator) });
                });
            }
        };
        const field = props.field;
        this.hasCustomField = field.options.fieldNames.length > 0;
        this.state = {
            label: "",
        };
    }
    componentDidMount() {
        this._loadData();
    }
    UNSAFE_componentWillReceiveProps() {
        this._loadData();
    }
    render() {
        return (React.createElement("div", null, this.state.label));
    }
}
Storage.set(Field.CascadeSelect.Type, (option) => React.createElement(CascadeSelect, Object.assign({}, option)));
exports.default = CascadeSelect;
