import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';
declare class CharSelect extends React.PureComponent<Storage.Option> {
    render(): JSX.Element | null;
    _renderLine: (item: Field.CharSelect.OptionItem, key: number) => JSX.Element | null;
}
export default CharSelect;
