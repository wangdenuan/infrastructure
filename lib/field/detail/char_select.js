"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
class CharSelect extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._renderLine = (item, key) => {
            const field = this.props.field;
            const values = ["", item.fieldValue.toLowerCase(), item.fieldValue.toUpperCase()];
            const data = this.props.data;
            const fieldValue = data[field.name] + "";
            let value = "";
            if (fieldValue.indexOf(item.fieldValue.toLowerCase()) >= 0) {
                value = item.fieldValue.toLowerCase();
            }
            else if (fieldValue.indexOf(item.fieldValue.toUpperCase()) >= 0) {
                value = item.fieldValue.toUpperCase();
            }
            const vIndex = values.findIndex((i) => i === value);
            if (vIndex < 0) {
                return null;
            }
            return (React.createElement("div", { key: key, className: "xc-bit-select-view" }, item.fieldLabel + ": " + item.labels[vIndex]));
        };
    }
    render() {
        const field = this.props.field;
        const data = this.props.data;
        return data[field.name] === undefined ? null : (React.createElement("div", null, field.options.dataSource.map(this._renderLine)));
    }
}
Storage.set(Field.CharSelect.Type, (option) => React.createElement(CharSelect, Object.assign({}, option)));
exports.default = CharSelect;
