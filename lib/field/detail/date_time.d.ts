import * as React from 'react';
import * as Storage from './storage';
declare class DateTime extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
    _timestamp: () => string;
    _stringFormat: () => string;
}
export default DateTime;
