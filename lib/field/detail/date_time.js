"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const moment_1 = __importDefault(require("moment"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class DateTime extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._timestamp = () => {
            const field = this.props.field;
            const data = this.props.data;
            let layout = "";
            if (field.options.dateFormat) {
                layout = field.options.dateFormat;
            }
            else {
                const items = [];
                if (field.options.hasDate) {
                    items.push("YYYY-MM-DD");
                }
                if (field.options.hasTime) {
                    items.push("HH:mm:ss");
                }
                layout = items.join(" ");
            }
            let value = "";
            if (data[field.name] !== undefined && data[field.name] > 0) {
                value = moment_1.default(data[field.name] * 1000).format(layout);
            }
            return value;
        };
        this._stringFormat = () => {
            const field = this.props.field;
            const data = this.props.data;
            let value = "";
            if (data[field.name] !== undefined) {
                value = data[field.name];
            }
            return value;
        };
    }
    render() {
        const field = this.props.field;
        const value = field.options.useTimestamp ? this._timestamp() : this._stringFormat();
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value),
                Render.description(field.options.description, field.options.showTag));
    }
}
Storage.set(Field.DateTime.Type, (option) => React.createElement(DateTime, Object.assign({}, option)));
exports.default = DateTime;
