import * as React from 'react';
import * as Storage from './storage';
declare class DigitView extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
}
export default DigitView;
