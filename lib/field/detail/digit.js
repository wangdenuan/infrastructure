"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class DigitView extends React.PureComponent {
    render() {
        const field = this.props.field;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            const bit = Field.Digit.GetDotBit(field.options.step);
            if (bit > 0) {
                value = value + (data[field.name] * field.options.step).toFixed(bit);
            }
            else {
                value = value + data[field.name] * field.options.step;
            }
        }
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value),
                Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth),
                Render.description(field.options.description, field.options.showTag));
    }
}
Storage.set(Field.Digit.Type, (option) => React.createElement(DigitView, Object.assign({}, option)));
exports.default = DigitView;
