import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';
interface State {
    dataSource: Field.Select.OptionItem[] | undefined;
}
declare class Select extends React.PureComponent<Storage.Option, State> {
    constructor(props: Storage.Option);
    componentDidMount(): void;
    render(): JSX.Element | null;
}
export default Select;
