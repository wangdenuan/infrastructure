"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class Select extends React.PureComponent {
    constructor(props) {
        super(props);
        const field = this.props.field;
        this.state = {
            dataSource: field.options.dataSource,
        };
    }
    componentDidMount() {
        if (this.state.dataSource === undefined) {
            const field = this.props.field;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                    this.setState({ dataSource });
                });
            }
            else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }
    render() {
        if (this.state.dataSource === undefined) {
            return null;
        }
        const field = this.props.field;
        const data = this.props.data;
        const valueMap = this.state.dataSource
            .reduce((prv, cur) => {
            prv[cur.value] = cur.label;
            return prv;
        }, {});
        let value = "";
        if (data[field.name] !== undefined) {
            const v = data[field.name];
            const items = Array.isArray(v) ? v : [v];
            value = items
                .map(i => valueMap[i] !== undefined ? valueMap[i] : field.options.unknownLabel)
                .join(",");
        }
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value),
                Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth),
                Render.description(field.options.description, field.options.showTag));
    }
}
Storage.set(Field.Select.Type, (option) => React.createElement(Select, Object.assign({}, option)));
exports.default = Select;
