/// <reference types="react" />
import * as Field from '../../config';
export interface Option {
    /**
     * 字段信息。
     */
    field: Field.Base.DetailConfig;
    /**
     * 实际数据。
     */
    data: {
        [key: string]: any;
    };
}
export declare function get(option: Option): React.ReactNode | null;
export declare function set(fieldType: string, func?: (option: Option) => React.ReactNode | null): void;
