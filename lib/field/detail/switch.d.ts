import * as React from 'react';
import * as Storage from './storage';
declare class Switch extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
}
export default Switch;
