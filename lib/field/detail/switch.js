"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class Switch extends React.PureComponent {
    render() {
        const field = this.props.field;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value = data[field.name] ? field.options.trueLabel : field.options.falseLabel;
        }
        let b = false;
        if (data[field.name] !== undefined) {
            b = data[field.name];
        }
        const useDot = field.options.useDot;
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value),
                useDot
                    ? (b
                        ? React.createElement("span", { style: { width: "15px", height: "15px", backgroundColor: "green", borderRadius: "50%" } })
                        : React.createElement("span", { style: { width: "15px", height: "15px", backgroundColor: "red", borderRadius: "50%" } }))
                    : Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth),
                Render.description(field.options.description, field.options.showTag));
    }
    ;
}
Storage.set(Field.Switch.Type, (option) => React.createElement(Switch, Object.assign({}, option)));
exports.default = Switch;
