import * as React from 'react';
import * as Storage from './storage';
declare class TextView extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
}
export default TextView;
