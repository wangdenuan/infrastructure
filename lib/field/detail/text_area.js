"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
class TextView extends React.PureComponent {
    render() {
        const field = this.props.field;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value += data[field.name];
        }
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value));
    }
}
Storage.set(Field.TextArea.Type, (option) => React.createElement(TextView, Object.assign({}, option)));
exports.default = TextView;
