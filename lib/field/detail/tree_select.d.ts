import * as React from 'react';
import { TreeNodeNormal } from 'antd/lib/tree-select/interface';
import * as Storage from './storage';
interface State {
    tree: TreeNodeNormal[];
}
declare class TreeSelectView extends React.PureComponent<Storage.Option, State> {
    state: State;
    componentDidMount(): void;
    render(): JSX.Element;
    _loadData: () => void;
}
export default TreeSelectView;
