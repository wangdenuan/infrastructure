"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class TreeSelectView extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.state = {
            tree: [],
        };
        this._loadData = () => {
            const field = this.props.field;
            field.options.getDataSource()
                .then((tree) => {
                this.setState({ tree });
            })
                .catch(() => {
                this.setState({ tree: [] });
            });
        };
    }
    componentDidMount() {
        this._loadData();
    }
    render() {
        const field = this.props.field;
        const d = this.props.data[field.name];
        const data = Array.isArray(d) ? d : [d];
        const convert = function (i) {
            const tmp = [i];
            (i.children || []).forEach(j => {
                tmp.push(...convert(j));
            });
            return tmp;
        };
        const items = this.state.tree.reduce((prv, cur) => {
            return [...prv, ...convert(cur)];
        }, []);
        const value = data
            .map((item) => {
            const j = items.find(i => i.value === item);
            if (j) {
                return j.title;
            }
            else {
                return field.options.unknownLabel;
            }
        })
            .join(",");
        return value === ""
            ? React.createElement("div", { className: "xc-detail-content" })
            : React.createElement("div", { className: "xc-detail-content" },
                React.createElement("span", { style: { width: field.options.valueWidth || "auto" } }, value),
                Render.description(field.options.description, field.options.showTag));
    }
}
Storage.set(Field.TreeSelect.Type, (option) => React.createElement(TreeSelectView, Object.assign({}, option)));
exports.default = TreeSelectView;
