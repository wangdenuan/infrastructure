import * as React from 'react';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Field from '../../config';
declare class ArraySelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void;
    render(): JSX.Element;
    _renderItem: (option: Field.ArraySelect.OptionItem, idx: number) => JSX.Element;
    _onChange: (option: Field.ArraySelect.OptionItem, e: RadioChangeEvent) => void;
    _calculateResult: () => void;
    _getDefaultValue: (option: Field.ArraySelect.OptionItem) => number;
    _charFieldName: (option: Field.ArraySelect.OptionItem) => string;
}
export default ArraySelect;
