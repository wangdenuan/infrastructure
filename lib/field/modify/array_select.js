"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
class ArraySelect extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._renderItem = (option, idx) => {
            const field = this.props.field;
            const form = this.props.form;
            const values = [...option.values];
            return (React.createElement(antd_1.Row, { className: "xc-bit-select-row", key: idx.toString() },
                React.createElement(antd_1.Col, { span: field.options.colSpan }, option.fieldLabel),
                React.createElement(antd_1.Col, { span: 24 - field.options.colSpan },
                    React.createElement(antd_1.Form.Item, null, form.getFieldDecorator(this._charFieldName(option), {
                        rules: [{
                                required: field.options.required,
                                message: "请填写" + option.fieldLabel,
                            }],
                        initialValue: this._getDefaultValue(option),
                    })(React.createElement(antd_1.Radio.Group, { name: this._charFieldName(option), onChange: this._onChange.bind(this, option) }, values.map((v, i) => (React.createElement(antd_1.Radio, { key: v, value: v }, option.labels[i])))))))));
        };
        this._onChange = (option, e) => {
            const { form } = this.props;
            const value = e.target.value;
            form.setFieldsValue({
                [this._charFieldName(option)]: value,
            });
            this._calculateResult();
        };
        this._calculateResult = () => {
            const field = this.props.field;
            const form = this.props.form;
            // 计算最后结果
            const result = field.options.dataSource.reduce((prv, cur) => {
                let { value } = prv;
                let val = form.getFieldValue(this._charFieldName(cur)) + "";
                if (cur.preValue !== "") {
                    val = cur.preValue + "-" + val;
                }
                value.push(val);
                return { value };
            }, { value: new Array() });
            form.setFieldsValue({
                [field.name]: result.value,
            }, () => {
                this.props.onChange && this.props.onChange(field.name, result.value);
            });
        };
        this._getDefaultValue = (option) => {
            const initialValue = this.props.initialValue;
            if (initialValue === undefined) {
                return option.defaultValue;
            }
            else {
                let value = option.values[0];
                // 通过preValue找到对应的item
                const itemValue = initialValue.find((i) => { return i.startsWith(option.preValue + "-"); });
                if (!itemValue) {
                    return value;
                }
                // 切分itemValue获取单项的值
                const vals = itemValue.split("-");
                if (vals.length <= 1) {
                    return value;
                }
                const idx = option.values.indexOf(parseInt(vals[1], 10) || 0);
                if (idx >= 0) {
                    value = option.values[idx];
                }
                return value;
            }
        };
        this._charFieldName = (option) => {
            return Storage.Prefix + this.props.field.name + option.fieldLabel;
        };
    }
    componentDidMount() {
        this._calculateResult();
    }
    render() {
        const field = this.props.field;
        const label = field.options.label || field.label;
        const form = this.props.form;
        form.getFieldDecorator(field.name);
        return (React.createElement("div", null,
            React.createElement(antd_1.Form.Item, { label: label }, field.options.dataSource.map(this._renderItem))));
    }
}
Storage.set(Field.ArraySelect.Type, (option) => React.createElement(ArraySelect, Object.assign({}, option)));
exports.default = ArraySelect;
