import * as React from 'react';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Field from '../../config';
declare class BitSelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void;
    render(): JSX.Element;
    _renderItem: (option: Field.BitSelect.OptionItem, idx: number) => JSX.Element;
    _onChange: (option: Field.BitSelect.OptionItem, e: RadioChangeEvent) => void;
    _calculateResult: () => void;
    _getDefaultValue: (option: Field.BitSelect.OptionItem) => number;
    _bitFieldName: (option: Field.BitSelect.OptionItem) => string;
}
export default BitSelect;
