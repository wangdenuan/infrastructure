"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
// TODO 整体required
class BitSelect extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._renderItem = (option, idx) => {
            const field = this.props.field;
            const form = this.props.form;
            return (React.createElement(antd_1.Row, { className: "xc-bit-select-row", key: idx.toString() },
                React.createElement(antd_1.Col, { span: field.options.colSpan }, option.fieldLabel),
                React.createElement(antd_1.Col, { span: 24 - field.options.colSpan },
                    React.createElement(antd_1.Form.Item, null,
                        form.getFieldDecorator(this._bitFieldName(option), {
                            rules: [{
                                    required: field.options.required,
                                    message: "请填写" + option.fieldLabel,
                                }],
                            initialValue: this._getDefaultValue(option),
                        })(React.createElement(antd_1.Radio.Group, { name: this._bitFieldName(option), onChange: this._onChange.bind(this, option) }, option.values.map((v, i) => (React.createElement(antd_1.Radio, { key: v, value: v }, option.labels[i]))))),
                        Render.description(option.description, option.showTag === undefined ? true : option.showTag)))));
        };
        this._onChange = (option, e) => {
            const { form } = this.props;
            const value = e.target.value;
            form.setFieldsValue({
                [this._bitFieldName(option)]: value,
            });
            this._calculateResult();
        };
        this._calculateResult = () => {
            const field = this.props.field;
            const form = this.props.form;
            // 计算最后结果
            const items = field.options.dataSource.sort((a, b) => a.index < b.index ? -1 : 1);
            const result = items.reduce((prv, cur) => {
                let { base, index, count } = prv;
                while (index < cur.index) {
                    base = base * field.options.base;
                    index = index + 1;
                }
                const value = form.getFieldValue(this._bitFieldName(cur));
                count = count + base * value;
                return { base, index, count };
            }, { base: 1, index: 0, count: 0 });
            form.setFieldsValue({
                [field.name]: result.count,
            }, () => {
                this.props.onChange && this.props.onChange(field.name, result.count);
            });
        };
        this._getDefaultValue = (option) => {
            const field = this.props.field;
            const initialValue = this.props.initialValue;
            if (initialValue === undefined) {
                return option.defaultValue;
            }
            else {
                let value = initialValue, loc = option.index;
                while (loc > 0) {
                    loc = loc - 1;
                    value = Math.floor(value / field.options.base);
                }
                return value % field.options.base;
            }
        };
        this._bitFieldName = (option) => {
            return Storage.Prefix + this.props.field.name + option.index;
        };
    }
    componentDidMount() {
        this._calculateResult();
    }
    render() {
        const field = this.props.field;
        const label = field.options.label || field.label;
        const form = this.props.form;
        form.getFieldDecorator(field.name);
        return (React.createElement("div", null,
            React.createElement(antd_1.Form.Item, { label: label }, field.options.dataSource.map(this._renderItem))));
    }
}
Storage.set(Field.BitSelect.Type, (option) => React.createElement(BitSelect, Object.assign({}, option)));
exports.default = BitSelect;
