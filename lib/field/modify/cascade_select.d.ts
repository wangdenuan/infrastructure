import * as React from 'react';
import { CascaderOptionType } from 'antd/lib/cascader';
import * as Storage from './storage';
export interface State {
    options: CascaderOptionType[];
}
declare class CascadeSelect extends React.PureComponent<Storage.Option, State> {
    hasCustomField: boolean;
    constructor(props: Storage.Option);
    componentDidMount(): void;
    render(): JSX.Element;
    _loadData: (selectedOptions?: CascaderOptionType[]) => void;
    _onChange: (value: string[]) => void;
}
export default CascadeSelect;
