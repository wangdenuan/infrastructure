"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
class CascadeSelect extends React.PureComponent {
    constructor(props) {
        super(props);
        this._loadData = (selectedOptions = []) => {
            const options = [...this.state.options];
            const optLen = selectedOptions.length;
            let targetOption = optLen > 0 ? selectedOptions[optLen - 1] : null;
            if (targetOption && targetOption.children && targetOption.children.length > 0) {
                return;
            }
            const field = this.props.field;
            if (!field.options.getLevelOptions) {
                return;
            }
            targetOption && (targetOption.loading = true);
            field.options.getLevelOptions(selectedOptions)
                .then((result) => {
                if (!targetOption) {
                    options.push(...result);
                    return;
                }
                if (result.length === 0) {
                    targetOption.isLeaf = true;
                }
                else {
                    targetOption.children = result;
                }
            })
                .finally(() => {
                targetOption && (targetOption.loading = false);
                this.setState({ options });
            });
        };
        this._onChange = (value) => {
            const field = this.props.field;
            const { form, onChange } = this.props;
            if (this.hasCustomField) {
                const values = {};
                field.options.fieldNames.forEach((fieldName, fieldIndex) => {
                    values[fieldName] = fieldIndex < value.length ? value[fieldIndex] : undefined;
                });
                form.setFieldsValue(values, () => {
                    Object.keys(values).forEach(fieldName => {
                        onChange && onChange(fieldName, values[fieldName]);
                    });
                });
            }
            else {
                onChange && onChange(field.name, value);
            }
        };
        const field = props.field;
        this.hasCustomField = field.options.fieldNames.length > 0;
        this.state = {
            options: field.options.dataSource || [],
        };
    }
    componentDidMount() {
        this._loadData(this.state.options);
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        if (this.hasCustomField) {
            field.options.fieldNames.forEach(fieldName => {
                form.getFieldDecorator(fieldName);
            });
        }
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label }, form.getFieldDecorator(this.hasCustomField ? Storage.Prefix + field.name : field.name, {
            rules: [{
                    required: field.options.required,
                    message: "请填写" + label,
                }],
            initialValue: this.props.initialValue
        })(React.createElement(antd_1.Cascader, { options: this.state.options, changeOnSelect: true, placeholder: field.options.placeholder, style: { width: field.options.valueWidth || 500 }, loadData: !field.options.dataSource ? this._loadData : undefined, onChange: this._onChange }))));
    }
}
Storage.set(Field.CascadeSelect.Type, (option) => React.createElement(CascadeSelect, Object.assign({}, option)));
exports.default = CascadeSelect;
