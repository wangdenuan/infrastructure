import * as React from 'react';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Field from '../../config';
declare class CharSelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void;
    render(): JSX.Element;
    _renderItem: (option: Field.CharSelect.OptionItem, idx: number) => JSX.Element;
    _onChange: (option: Field.CharSelect.OptionItem, e: RadioChangeEvent) => void;
    _calculateResult: () => void;
    _getDefaultValue: (option: Field.CharSelect.OptionItem) => string;
    _charFieldName: (option: Field.CharSelect.OptionItem) => string;
}
export default CharSelect;
