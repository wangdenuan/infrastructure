import * as React from 'react';
import moment from 'moment';
import * as Storage from './storage';
declare class DateTime extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
    _onChange: (date: moment.Moment | null, _: string) => void;
    _timestampScale: () => 1 | 1000;
    _layout: () => string;
}
export default DateTime;
