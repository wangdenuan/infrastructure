"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const moment_1 = __importDefault(require("moment"));
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class DateTime extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._onChange = (date, _) => {
            const field = this.props.field;
            const form = this.props.form;
            if (date === null) {
                if (field.options.useTimestamp) {
                    form.setFieldsValue({
                        [field.name]: 0,
                    });
                }
                else {
                    form.setFieldsValue({
                        [field.name]: "",
                    });
                }
            }
            else {
                if (field.options.useTimestamp) {
                    form.setFieldsValue({
                        [field.name]: Math.floor(date.valueOf() / this._timestampScale()),
                    });
                }
                else {
                    form.setFieldsValue({
                        [field.name]: date.format(this._layout()),
                    });
                }
            }
        };
        this._timestampScale = () => {
            const field = this.props.field;
            return field.options.useSecond ? 1000 : 1;
        };
        this._layout = () => {
            const field = this.props.field;
            let layout = field.options.dateFormat;
            if (layout === undefined) {
                const items = [];
                if (field.options.hasDate) {
                    items.push("YYYY-MM-DD");
                }
                if (field.options.hasTime) {
                    items.push("HH:mm:ss");
                }
                layout = items.join(" ");
            }
            return layout;
        };
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const onlyTime = !field.options.hasDate && field.options.hasTime;
        form.getFieldDecorator(field.name, {
            initialValue: this.props.initialValue,
        });
        let initialValue = undefined;
        if (this.props.initialValue) {
            if (field.options.useTimestamp) {
                initialValue = moment_1.default(this.props.initialValue * this._timestampScale());
            }
            else {
                initialValue = moment_1.default(this.props.initialValue, this._layout());
            }
        }
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label },
            form.getFieldDecorator(Storage.Prefix + field.name, {
                initialValue: initialValue,
                rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }]
            })(onlyTime ? (React.createElement(antd_1.TimePicker, { placeholder: field.options.placeholder, format: field.options.dateFormat, style: { width: field.options.valueWidth || 200 }, onChange: this._onChange })) : (React.createElement(antd_1.DatePicker, { showTime: field.options.hasTime, placeholder: field.options.placeholder, format: field.options.dateFormat, style: { width: field.options.valueWidth || 200 }, onChange: this._onChange }))),
            Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)));
    }
}
Storage.set(Field.DateTime.Type, (option) => React.createElement(DateTime, Object.assign({}, option)));
exports.default = DateTime;
