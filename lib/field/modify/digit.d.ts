import * as React from 'react';
import * as Storage from './storage';
declare class Digit extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
    _onChange: (value: number | undefined) => void;
}
export default Digit;
