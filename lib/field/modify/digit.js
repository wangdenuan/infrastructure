"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
// TODO 校验整数、小数位数。
class Digit extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._onChange = (value) => {
            if (value === undefined) {
                return;
            }
            const field = this.props.field;
            const form = this.props.form;
            const bit = Field.Digit.GetDotBit(field.options.step);
            if (bit > 0) {
                form.setFieldsValue({
                    [field.name]: parseFloat((value / field.options.step).toFixed(bit)),
                });
            }
            else {
                form.setFieldsValue({
                    [field.name]: value / field.options.step,
                });
            }
        };
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        form.getFieldDecorator(field.name, {
            initialValue: this.props.initialValue,
        });
        let initialValue = 0;
        if (this.props.initialValue) {
            const bit = Field.Digit.GetDotBit(field.options.step);
            if (bit > 0) {
                initialValue = parseFloat((this.props.initialValue * field.options.step).toFixed(bit));
            }
            else {
                initialValue = this.props.initialValue * field.options.step;
            }
        }
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label },
            form.getFieldDecorator(Storage.Prefix + field.name, {
                rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                initialValue: initialValue,
            })(React.createElement(antd_1.InputNumber, { max: field.options.max, min: field.options.min, step: field.options.step, style: { width: field.options.valueWidth || 200 }, onChange: this._onChange })),
            Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth),
            Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)));
    }
}
Storage.set(Field.Digit.Type, (option) => React.createElement(Digit, Object.assign({}, option)));
exports.default = Digit;
