import * as React from 'react';
import { SelectValue } from "antd/lib/select";
import * as Storage from './storage';
import * as Field from '../../config';
interface State {
    dataSource: Field.Select.OptionItem[] | undefined;
}
declare class SelectView extends React.PureComponent<Storage.Option, State> {
    constructor(props: Storage.Option);
    componentDidMount(): void;
    render(): JSX.Element;
    _renderSelectOption: (opt: Field.Select.OptionItem) => JSX.Element;
    _onChange: (value: SelectValue) => void;
}
export default SelectView;
