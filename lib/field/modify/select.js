"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
// TODO limitCount支持
class SelectView extends React.PureComponent {
    constructor(props) {
        super(props);
        this._renderSelectOption = (opt) => {
            return (React.createElement(antd_1.Select.Option, { key: opt.value, value: opt.value }, opt.label));
        };
        this._onChange = (value) => {
            const { field, onChange } = this.props;
            onChange && onChange(field.name, value);
        };
        const field = this.props.field;
        this.state = {
            dataSource: field.options.dataSource,
        };
    }
    componentDidMount() {
        if (this.state.dataSource === undefined) {
            const field = this.props.field;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                    this.setState({ dataSource });
                });
            }
            else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const mul = field.options.multiple;
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label, hasFeedback: !mul },
            form.getFieldDecorator(field.name, {
                initialValue: this.props.initialValue,
                rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                        type: mul ? "array" : field.options.valueType,
                    }],
            })(React.createElement(antd_1.Select, { mode: mul ? "multiple" : "default", placeholder: field.options.placeholder, style: { width: field.options.valueWidth || 200 }, showSearch: field.options.filtered, showArrow: field.options.filtered, filterOption: field.options.filtered
                    ? (value, option) => {
                        return option.props.children.indexOf(value) >= 0;
                    }
                    : undefined, onChange: this._onChange }, this.state.dataSource !== undefined ? this.state.dataSource.map(this._renderSelectOption) : undefined)),
            Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth),
            Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)));
    }
}
Storage.set(Field.Select.Type, (option) => React.createElement(SelectView, Object.assign({}, option)));
exports.default = SelectView;
