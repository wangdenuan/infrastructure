import * as React from 'react';
import * as Storage from './storage';
declare class SwitchView extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
    _onRadioChange: (e: any) => void;
    _onSwitchChange: (value: boolean) => void;
}
export default SwitchView;
