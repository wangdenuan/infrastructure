"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class SwitchView extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._onRadioChange = (e) => {
            const { field, onChange } = this.props;
            onChange && onChange(field.name, e.target.value);
        };
        this._onSwitchChange = (value) => {
            const { field, onChange } = this.props;
            onChange && onChange(field.name, value);
        };
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const useRadio = field.options.useRadio;
        const initialValue = this.props.initialValue || false;
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label },
            form.getFieldDecorator(field.name, {
                initialValue: initialValue,
                valuePropName: "checked",
                rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
            })(useRadio
                ? React.createElement(antd_1.Radio.Group, { name: field.name, defaultValue: initialValue, style: { width: field.options.valueWidth || 200 }, onChange: this._onRadioChange },
                    React.createElement(antd_1.Radio, { value: true }, field.options.trueLabel),
                    React.createElement(antd_1.Radio, { value: false }, field.options.falseLabel))
                : React.createElement(antd_1.Switch, { defaultChecked: initialValue, checkedChildren: field.options.trueLabel, unCheckedChildren: field.options.falseLabel, style: { width: field.options.valueWidth || "auto" }, onChange: this._onSwitchChange })),
            useRadio ? null : Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth),
            Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)));
    }
}
Storage.set(Field.Switch.Type, (option) => React.createElement(SwitchView, Object.assign({}, option)));
exports.default = SwitchView;
