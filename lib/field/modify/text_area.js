"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
const { TextArea } = antd_1.Input;
// TODO numberOfLines支持
class TextView extends React.PureComponent {
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label }, form.getFieldDecorator(field.name, {
            rules: [{
                    required: field.options.required,
                    message: "请填写" + label,
                    max: field.options.maxLength > 0 ? field.options.maxLength : undefined,
                }],
            initialValue: this.props.initialValue || "",
        })(React.createElement(TextArea, { placeholder: field.options.placeholder, style: { width: field.options.valueWidth || 200, height: field.options.valueHeight || 60 }, autoSize: { minRows: field.options.minRows || 2, maxRows: field.options.maxRows || 6 } }))));
    }
}
Storage.set(Field.TextArea.Type, (option) => React.createElement(TextView, Object.assign({}, option)));
exports.default = TextView;
