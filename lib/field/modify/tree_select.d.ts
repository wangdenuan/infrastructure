import * as React from 'react';
import { TreeNodeNormal, TreeNodeValue } from "antd/lib/tree-select/interface";
import * as Storage from './storage';
export interface State {
    tree: TreeNodeNormal[];
}
declare class TreeSelectView extends React.PureComponent<Storage.Option, State> {
    state: State;
    componentDidMount(): void;
    render(): JSX.Element;
    _loadData: () => void;
    _onChange: (value: TreeNodeValue) => void;
}
export default TreeSelectView;
