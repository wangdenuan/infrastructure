"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Render = __importStar(require("../util/render"));
const Field = __importStar(require("../../config"));
class TreeSelectView extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.state = {
            tree: [],
        };
        this._loadData = () => {
            const field = this.props.field;
            field.options.getDataSource()
                .then((tree) => {
                this.setState({ tree });
            })
                .catch(() => {
                this.setState({ tree: [] });
            });
        };
        this._onChange = (value) => {
            const { field, onChange } = this.props;
            onChange && onChange(field.name, value);
        };
    }
    componentDidMount() {
        this._loadData();
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        return (React.createElement(antd_1.Form.Item, { className: "xc-modify-content", label: label },
            form.getFieldDecorator(field.name, {
                rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                initialValue: this.props.initialValue,
            })(React.createElement(antd_1.TreeSelect, { dropdownStyle: {
                    maxHeight: field.options.maxHeight,
                    overflow: 'auto',
                }, multiple: field.options.multiple, treeData: this.state.tree, placeholder: field.options.placeholder, treeDefaultExpandAll: field.options.defaultExpandAll, style: { width: field.options.valueWidth || 500 }, onChange: this._onChange })),
            Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)));
    }
}
Storage.set(Field.TreeSelect.Type, (option) => React.createElement(TreeSelectView, Object.assign({}, option)));
exports.default = TreeSelectView;
