import * as React from 'react';
import { RangePickerValue } from 'antd/lib/date-picker/interface';
import * as Storage from './storage';
declare class DateTime extends React.PureComponent<Storage.Option> {
    render(): JSX.Element;
    _onChange: (dates: RangePickerValue) => void;
    _timestampScale: () => 1 | 1000;
    _leftFieldName: () => string;
    _rightFieldName: () => string;
}
export default DateTime;
