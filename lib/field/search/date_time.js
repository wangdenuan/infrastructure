"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const moment_1 = __importDefault(require("moment"));
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
const util_1 = require("../../util");
const { RangePicker } = antd_1.DatePicker;
class DateTime extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._onChange = (dates) => {
            const field = this.props.field;
            const { form, onChange } = this.props;
            if (!dates || dates.length != 2) {
                form.setFieldsValue({
                    [this._leftFieldName()]: undefined,
                    [this._rightFieldName()]: undefined,
                });
                return;
            }
            if (dates[0]) {
                const c = {
                    left: field.name,
                    right: Math.floor(dates[0].valueOf() / this._timestampScale()),
                    op: util_1.Condition.OpGreaterThanEqual,
                };
                form.setFieldsValue({
                    [this._leftFieldName()]: c,
                });
            }
            if (dates[1]) {
                const c = {
                    left: field.name,
                    right: Math.floor(dates[1].valueOf() / this._timestampScale()),
                    op: util_1.Condition.OpLessThan,
                };
                form.setFieldsValue({
                    [this._rightFieldName()]: c,
                });
            }
            onChange && onChange(field.name, dates);
        };
        this._timestampScale = () => {
            const field = this.props.field;
            return field.options.useSecond ? 1000 : 1;
        };
        this._leftFieldName = () => {
            return this.props.field.name + "_left";
        };
        this._rightFieldName = () => {
            return this.props.field.name + "_right";
        };
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        const label = field.options.label || field.label;
        form.getFieldDecorator(this._leftFieldName(), {});
        form.getFieldDecorator(this._rightFieldName(), {});
        const startTime = field.options.startTimeValue > 0 ? moment_1.default(field.options.startTimeValue * this._timestampScale()) : undefined;
        const endTime = field.options.endTimeValue > 0 ? moment_1.default(field.options.endTimeValue * this._timestampScale()) : undefined;
        return (React.createElement(antd_1.Form.Item, { label: label }, form.getFieldDecorator(Storage.Prefix + field.name, {
            initialValue: [startTime, endTime],
        })(React.createElement(RangePicker, { onChange: this._onChange, format: field.options.dateFormat, showTime: field.options.hasTime, style: { width: "100%" } }))));
    }
}
Storage.set(Field.DateTime.Type, (option) => React.createElement(DateTime, Object.assign({}, option)));
exports.default = DateTime;
