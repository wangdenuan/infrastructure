"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
const util_1 = require("../../util");
class SelectView extends React.PureComponent {
    constructor(props) {
        super(props);
        this._renderSelectOption = (opt) => {
            return (React.createElement(antd_1.Select.Option, { key: opt.value, value: opt.value }, opt.label));
        };
        this._onChange = (value) => {
            const { field, form, onChange } = this.props;
            const c = {
                left: field.name,
                right: value,
                op: field.options.multiple ? util_1.Condition.OpIn : util_1.Condition.OpEqual,
            };
            form.setFieldsValue({
                [field.name]: c,
            });
            onChange && onChange(field.name, value);
        };
        const field = this.props.field;
        this.state = {
            value: [],
            dataSource: field.options.dataSource,
        };
    }
    componentDidMount() {
        if (this.state.dataSource === undefined) {
            const field = this.props.field;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                    this.setState({ dataSource });
                });
            }
            else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        form.getFieldDecorator(field.name, {});
        const label = field.options.label || field.label;
        const mul = field.options.multiple;
        return (React.createElement(antd_1.Form.Item, { label: label }, form.getFieldDecorator(Storage.Prefix + field.name, {
            initialValue: field.options.defaultValue,
            rules: [{
                    type: mul ? "array" : field.options.valueType,
                }],
        })(React.createElement(antd_1.Select, { mode: mul ? "multiple" : "default", placeholder: field.options.placeholder, onChange: this._onChange }, this.state.dataSource !== undefined ? this.state.dataSource.map(this._renderSelectOption) : undefined))));
    }
}
Storage.set(Field.Select.Type, (option) => React.createElement(SelectView, Object.assign({}, option)));
exports.default = SelectView;
