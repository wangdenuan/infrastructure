/// <reference types="react" />
import { WrappedFormUtils } from 'antd/lib/form/Form';
import * as Field from '../../config';
export declare const Prefix = "__";
export interface Option {
    /**
     * 字段信息。
     */
    field: Field.Base.SearchConfig;
    /**
     * 表格操作。
     */
    form: WrappedFormUtils;
    /**
     * 字段改变的回调函数。
     */
    onChange?: (name: string, value: any) => void;
}
export declare function get(option: Option): React.ReactNode | null;
export declare function set(fieldType: string, func?: (option: Option) => React.ReactNode | null): void;
