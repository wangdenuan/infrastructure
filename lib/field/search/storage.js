"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Prefix = "__";
const gMap = {};
function get(option) {
    const fieldType = option.field.type;
    if (gMap[fieldType]) {
        return gMap[fieldType](option);
    }
    else {
        return null;
    }
}
exports.get = get;
function set(fieldType, func) {
    if (func) {
        gMap[fieldType] = func;
    }
    else {
        delete gMap[fieldType];
    }
}
exports.set = set;
