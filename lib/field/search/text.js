"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Storage = __importStar(require("./storage"));
const Field = __importStar(require("../../config"));
const util_1 = require("../../util");
class TextView extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._onChange = (event) => {
            const { value } = event.target;
            const field = this.props.field;
            const { form, onChange } = this.props;
            if (value === "") {
                form.setFieldsValue({
                    [field.name]: undefined,
                });
            }
            else {
                const c = {
                    left: field.name,
                    right: value,
                    op: util_1.Condition.OpLike,
                };
                form.setFieldsValue({
                    [field.name]: c,
                });
            }
            onChange && onChange(field.name, value);
        };
    }
    render() {
        const field = this.props.field;
        const form = this.props.form;
        form.getFieldDecorator(field.name, {});
        const label = field.options.label || field.label;
        return (React.createElement(antd_1.Form.Item, { label: label }, form.getFieldDecorator(Storage.Prefix + field.name, {
            rules: [{
                    max: field.options.maxLength > 0 ? field.options.maxLength : undefined,
                }],
            initialValue: field.options.defaultValue,
        })(React.createElement(antd_1.Input, { placeholder: field.options.placeholder, onChange: this._onChange }))));
    }
}
Storage.set(Field.Text.Type, (option) => React.createElement(TextView, Object.assign({}, option)));
exports.default = TextView;
