"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
function unit(unit, tag, witdh) {
    return unit
        ? (tag === true
            ? React.createElement(antd_1.Tag, { color: "#2db7f5", style: { width: witdh || "auto", textAlign: "center" } }, unit)
            : React.createElement("span", null, unit)) : null;
}
exports.unit = unit;
function description(desc, tag) {
    return desc
        ? (tag === true
            ? React.createElement(antd_1.Tag, { color: "blue" }, desc)
            : React.createElement("span", null, unit)) : null;
}
exports.description = description;
