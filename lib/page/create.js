"use strict";
/**
 * 通用的新建页。
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const DetailFields = __importStar(require("../field/detail"));
const ModifyFields = __importStar(require("../field/modify"));
class CreateComponent extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.defaultFormProps = {
            layout: "horizontal",
            labelCol: {
                xs: { span: 4 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 20 },
                sm: { span: 20 },
            },
        };
        this.defaultSubmitFormItemProps = {
            labelCol: {
                xs: { span: 4 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 20 },
                sm: { span: 20 },
            },
        };
        // 渲染单个字段
        this._renderField = (field) => {
            if (field.options.readonly) {
                const option = {
                    field: Object.assign(Object.assign({}, field), { options: Object.assign(Object.assign({}, field.options), { showInDetail: true }) }),
                    data: field.options.defaultValue,
                };
                const label = field.options.label || field.label;
                return (React.createElement(antd_1.Form.Item, { style: { height: "auto" }, key: field.name, label: label }, DetailFields.get(option)));
            }
            else {
                const option = {
                    field,
                    form: this.props.form,
                    initialValue: field.options.defaultValue,
                    onChange: this.props.onChangeField,
                };
                const CreateFieldNode = ModifyFields.get(option);
                return (React.createElement("div", { key: field.name }, CreateFieldNode));
            }
        };
        // 渲染按钮
        this._renderSubmit = () => {
            return (React.createElement(antd_1.Form.Item, Object.assign({ className: "xc-form-button", label: " ", colon: false, style: { height: "auto" } }, this.defaultSubmitFormItemProps, this.props.submitFormItemProps),
                React.createElement(antd_1.Button, { className: "xc-button-default", icon: "check", type: "primary", htmlType: "submit" }, this.props.submitLabel),
                this.props.action && this.props.action.map((node) => {
                    return node;
                }),
                React.createElement(antd_1.Button, { className: "xc-button-reset", icon: "redo", type: "default", onClick: this._onReset }, "重置"),
                this.props.actionDesc
                    ? this.props.descType === undefined
                        ? this.props.actionDesc
                        : React.createElement(antd_1.Alert, { type: this.props.descType, message: this.props.actionDesc, showIcon: true })
                    : null));
        };
        // 提交表格
        this._onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFields(async (err, values) => {
                if (err) {
                    return;
                }
                const process = (item) => {
                    return Object.keys(item).reduce((prv, cur) => {
                        const v = item[cur];
                        if (v && typeof v === 'object' && !Array.isArray(v)) {
                            prv[cur] = process(v);
                        }
                        else if (cur.indexOf(ModifyFields.Prefix) !== 0) {
                            prv[cur] = v;
                        }
                        return prv;
                    }, {});
                };
                const newValue = process(values);
                console.log(newValue);
                this.props.commitData(newValue)
                    .catch((err) => {
                    antd_1.message.error(err.message);
                });
            });
        };
        // 重置表格
        this._onReset = (e) => {
            e.preventDefault();
            this.props.form.resetFields();
        };
    }
    render() {
        const fields = this.props.fields.filter(i => i.options.showInCreate);
        return (React.createElement(antd_1.Form, Object.assign({ className: "xc-create-form", onSubmit: this._onSubmit }, this.defaultFormProps, this.props.formProps),
            fields.map(this._renderField),
            this._renderSubmit()));
    }
}
CreateComponent.defaultProps = {
    submitLabel: "提交",
    formProps: {},
    submitFormItemProps: {},
};
exports.default = antd_1.Form.create()(CreateComponent);
