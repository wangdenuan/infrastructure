/**
 * 通用的详情页。
 */
import * as React from 'react';
import { DescriptionsProps } from 'antd/lib/descriptions';
import * as Field from '../config';
interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.DetailConfig[];
    /**
     * 展示的主标题。
     */
    title: string;
    /**
     * 详情数据。
     */
    data: {
        [key: string]: any;
    };
    /**
     * 主标题宽度。
     */
    titleWidth?: number;
    /**
     * 字段标题宽度。
     */
    labelWidth?: number;
    /**
     * 字段内容宽度。
     */
    dataWidth?: number;
    /**
     * 详情组件的自定义属性。
     */
    descriptionProps?: DescriptionsProps;
    /**
     * 操作区域。
     */
    action?: React.ReactNode;
}
export default class extends React.PureComponent<Props> {
    render(): JSX.Element;
    _renderWithTitle: () => JSX.Element;
    _renderWithoutTitle: () => JSX.Element;
    _renderField: (field: Field.Base.DetailConfig) => React.ReactNode[] | JSX.Element;
    _renderAction: () => JSX.Element | null;
}
export {};
