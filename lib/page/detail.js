"use strict";
/**
 * 通用的详情页。
 */
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const Field = __importStar(require("../config"));
const DetailFields = __importStar(require("../field/detail"));
const { TabPane } = antd_1.Tabs;
class default_1 extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._renderWithTitle = () => {
            return (React.createElement(antd_1.Tabs, { className: "xc-tab", type: "card" },
                React.createElement(TabPane, { tab: (React.createElement("div", { style: { minWidth: this.props.titleWidth || 118 } }, this.props.title)), key: "1" }, this._renderWithoutTitle())));
        };
        this._renderWithoutTitle = () => {
            const fields = this.props.fields.filter(i => i.options.showInDetail);
            return (React.createElement("div", null,
                React.createElement(antd_1.Descriptions, Object.assign({ size: "small", layout: "horizontal", bordered: true }, this.props.descriptionProps), fields.map(this._renderField)),
                this._renderAction()));
        };
        this._renderField = (field) => {
            const labelWidth = this.props.labelWidth || 118;
            let dataWidth = this.props.dataWidth || "auto";
            if (field.options.span && field.options.span > 1) {
                if (this.props.dataWidth) {
                    dataWidth = this.props.dataWidth * field.options.span + labelWidth * (field.options.span - 1);
                }
                else {
                    dataWidth = labelWidth * (2 * field.options.span - 1);
                }
            }
            if (field.type === Field.BitSelect.Type && field.options.row) {
                // BitSelect且设置row=true则返回多个Descriptions.Item
                const items = [];
                field.options.dataSource.forEach((item, idx) => {
                    const option = {
                        field: Object.assign(Object.assign({}, field), { options: Object.assign(Object.assign({}, field.options), { itemIndex: idx }) }),
                        data: this.props.data,
                    };
                    const DetailFieldNode = DetailFields.get(option);
                    items.push(React.createElement(antd_1.Descriptions.Item, { key: idx, label: (React.createElement("div", { style: { width: labelWidth } }, item.fieldLabel)), span: field.options.span },
                        React.createElement("div", { style: { width: dataWidth } }, DetailFieldNode)));
                });
                return items;
            }
            else {
                // 返回单个Descriptions.Item
                const option = { field, data: this.props.data };
                const label = field.options.label || field.label;
                const DetailFieldNode = DetailFields.get(option);
                return (React.createElement(antd_1.Descriptions.Item, { key: field.name, label: (React.createElement("div", { style: { width: labelWidth } }, label)), span: field.options.span },
                    React.createElement("div", { style: { width: dataWidth } }, DetailFieldNode)));
            }
        };
        // 渲染按钮
        this._renderAction = () => {
            const labelWidth = this.props.labelWidth || 118;
            return this.props.action
                ? React.createElement(antd_1.Descriptions, { className: "xc-detail-button", size: "small", layout: "horizontal", bordered: true },
                    React.createElement(antd_1.Descriptions.Item, { label: (React.createElement("div", { style: { width: labelWidth } }, " ")) }, this.props.action))
                : null;
        };
    }
    render() {
        return this.props.title === ""
            ? this._renderWithoutTitle()
            : this._renderWithTitle();
    }
}
exports.default = default_1;
