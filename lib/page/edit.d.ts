/**
 * 通用的编辑页。
 */
import * as React from 'react';
import { FormItemProps } from "antd/lib/form";
import { AlertProps } from 'antd/lib/alert/index';
import { FormComponentProps, FormProps } from 'antd/lib/form/Form';
import * as Field from '../config';
interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.EditConfig[];
    /**
     * 数据。
     */
    data: {
        [key: string]: any;
    };
    /**
     * 提交按钮的自定义标签。
     */
    submitLabel: string;
    /**
     * 提交数据。
     * @param values 数据值
     */
    commitData: (values: any) => Promise<void>;
    /**
     * 字段值发生改变时的回调方法。
     * @param name 字段名称
     * @param value 字段内容
     */
    onChangeField?: (name: string, value: any) => void;
    /**
     * 表单的配置属性。
     */
    formProps: FormProps;
    /**
     * 提交按钮的表单Item配置属性。
     */
    submitFormItemProps: FormItemProps;
    /**
     * 操作按钮。
     */
    action?: React.ReactNode[];
    /**
    * 操作说明。
    */
    actionDesc?: string;
    /**
     * 描述展示类型。
     */
    descType?: AlertProps["type"];
}
interface EditFormProps extends FormComponentProps, Props {
}
declare class EditComponent extends React.PureComponent<EditFormProps> {
    static defaultProps: {
        submitLabel: string;
        formProps: {};
        submitFormItemProps: {};
    };
    defaultFormProps: FormProps;
    defaultSubmitFormItemProps: FormItemProps;
    render(): JSX.Element;
    _renderField: (field: Field.Base.EditConfig) => JSX.Element;
    _renderSubmit: () => JSX.Element;
    _onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
    _onReset: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}
declare const _default: import("antd/lib/form/interface").ConnectedComponentClass<typeof EditComponent, Pick<EditFormProps, "data" | "action" | "fields" | "wrappedComponentRef" | "submitLabel" | "commitData" | "onChangeField" | "formProps" | "submitFormItemProps" | "actionDesc" | "descType">>;
export default _default;
