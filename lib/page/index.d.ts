import List from './list';
import Create from './create';
import Edit from './edit';
import Detail from './detail';
import Search from './search';
export { List, Create, Edit, Detail, Search, };
