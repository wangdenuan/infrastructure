/**
 * 通用的列表页。
 */
import * as React from 'react';
import { PaginationConfig } from 'antd/lib/pagination';
import { TableEventListeners, TableProps, ColumnProps } from 'antd/lib/table';
import * as Field from '../config';
export interface FetchDataResult {
    /**
     * 数据。
     */
    data: Array<{
        [key: string]: any;
    }>;
    /**
     * 总页数。
     */
    count: number;
}
export interface Action {
    /**
     * 文字内容。
     */
    label?: React.ReactNode | ((data: any, mainKey: string) => React.ReactNode);
    /**
     * 是否启用。
     */
    enable?: (data: any, mainKey: string) => boolean;
    /**
     * 点击事件。
     */
    onClick?: (data: any, mainKey: string) => Promise<void> | void;
    /**
     * 弹框模式的点击事件。
     */
    modalClick?: (data: any, mainKey: string) => Promise<void>;
    /**
    * 弹框模式的标题栏(优先modalField)。
    */
    modalTitle?: string;
    /**
    * 弹框模式标题栏的取值字段(如果modalField和modalTitle都未设置，则不是弹框)。
    */
    modalField?: string;
    /**
    * 弹框模式的内容。
    */
    modalContent?: string;
    /**
   * 弹框模式的操作成功提示。
   */
    modalSuccessInfo?: string;
    /**
   * 弹框模式的操作错误提示。
   */
    modalErrorInfo?: string;
}
interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.ListConfig[];
    /**
     * 页大小，undefined表示不分页。
     */
    pageSize?: number;
    /**
     * 数据主键名称。
     */
    mainKey: string;
    /**
     * 分页获取列表数据。
     */
    fetchData: (pageNumber: number, pageSize: number, filters?: any, sorter?: any) => Promise<FetchDataResult>;
    /**
     * 行点击事件。
     */
    onRow?: (record: any, index: number) => TableEventListeners;
    /**
     * 选择行事件。
     */
    onSelectRows?: (selectedKeys: any[], isAll: boolean) => void;
    /**
     * 右侧编辑操作。
     */
    editAction?: Action;
    /**
     * 右侧删除操作。
     */
    deleteAction?: Action;
    /**
     * 右侧其他操作。
     */
    otherActions: Action[];
    /**
     * 操作列的附加属性。
     */
    actionColumnProps?: ColumnProps<any>;
    /**
     * 附加的属性。
     */
    tableProps?: TableProps<any>;
    /**
     * 是否自动加载。
     */
    autoLoad: boolean;
}
interface State {
    /**
     * 是否正在加载。
     */
    loading: boolean;
    /**
     * 实际数据。
     */
    data: Array<{
        [key: string]: any;
    }>;
    /**
     * 选择行。
     */
    selectedKeys: any[];
    /**
     * 当前页码。
     */
    pageNumber: number;
    /**
     * 总页码
     */
    pageCount: number;
    /**
     * 过滤条件。
     */
    filters: any;
    /**
     * 排序规则。
     */
    sorter: any;
}
export default class ListPage extends React.PureComponent<Props, State> {
    static defaultProps: {
        otherActions: never[];
        actionColumnProps: {};
        tableProps: {};
        autoLoad: boolean;
    };
    state: State;
    componentDidMount(): void;
    render(): JSX.Element;
    reload: (pageNumber?: number | undefined) => void;
    _pagination: () => false | {
        current: number;
        total: number;
        pageSize: number;
        showTotal: (total: number) => string;
    };
    _onTableChange: (pagination: PaginationConfig, filters: any, sorter: any) => void;
    _fetchData: (pageNumber: number, filters: any, sorter: any) => void;
    _onSelectRows: (selectedKeys: any[]) => void;
    _renderColumns: () => JSX.Element[];
    _actions: () => Action[];
    _renderColumnAction: (text: string, record: {
        [key: string]: any;
    }) => JSX.Element;
    _getActionLabel: (action: Action, defaultValue: string, data: any, mainKey: string) => any;
    _onModalClick: (act: Action, data: any, mainKey: string) => void;
    _onDeleteClick: (data: any, mainKey: string) => void;
    _onAction: (action: (data: any, mainKey: string) => Promise<void>, data: any, mainKey: string, pn: number, success: string, error: string) => void;
}
export {};
