"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 通用的列表页。
 */
const React = __importStar(require("react"));
const antd_1 = require("antd");
const DetailFields = __importStar(require("../field/detail"));
class ListPage extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.state = {
            loading: false,
            data: [],
            selectedKeys: [],
            pageNumber: 1,
            pageCount: 0,
            filters: {},
            sorter: {},
        };
        this.reload = (pageNumber) => {
            const n = pageNumber === undefined ? this.state.pageNumber : pageNumber;
            this._fetchData(n, this.state.filters, this.state.sorter);
        };
        // 分页信息
        this._pagination = () => {
            return this.props.pageSize === undefined ? false : {
                current: this.state.pageNumber,
                total: this.state.pageCount,
                pageSize: this.props.pageSize,
                showTotal: (total) => "共 " + total + " 条",
            };
        };
        // 表格切换页的回调
        this._onTableChange = (pagination, filters, sorter) => {
            const pageNumber = pagination.current;
            this._fetchData(pageNumber, filters, sorter);
        };
        // 刷新数据
        this._fetchData = (pageNumber, filters, sorter) => {
            this.setState({ loading: true });
            this.props.fetchData(pageNumber, this.props.pageSize || 0, Object.keys(filters).length === 0 ? undefined : filters, Object.keys(sorter).length === 0 ? undefined : sorter)
                .then((result) => {
                this.setState({
                    loading: false,
                    data: result.data,
                    pageCount: result.count,
                    pageNumber: pageNumber,
                    filters: filters,
                    sorter: sorter,
                });
            })
                .catch((err) => {
                this.setState({ loading: false }, () => {
                    antd_1.message.error(err.message);
                });
            });
        };
        // 选择行
        this._onSelectRows = (selectedKeys) => {
            this.setState({ selectedKeys: selectedKeys }, () => {
                this.props.onSelectRows && this.props.onSelectRows(selectedKeys, this.state.data.length === selectedKeys.length);
            });
        };
        // 渲染表格列
        this._renderColumns = () => {
            const columns = this.props.fields
                .filter(i => i.options.showInList)
                .map((fieldItem) => {
                return (React.createElement(antd_1.Table.Column, Object.assign({ key: fieldItem.name, dataIndex: fieldItem.name, title: fieldItem.options.label || fieldItem.label, sorter: fieldItem.options.tableProps && fieldItem.options.tableProps.sorter ? true : false, filters: fieldItem.options.filters, filterMultiple: false, render: (value) => {
                        const option = {
                            field: Object.assign(Object.assign({}, fieldItem), { options: Object.assign(Object.assign({}, fieldItem.options), { showInDetail: true }) }),
                            data: { [fieldItem.name]: value },
                        };
                        return DetailFields.get(option);
                    } }, fieldItem.options.tableProps)));
            });
            if (this._actions().length > 0) {
                columns.push((React.createElement(antd_1.Table.Column, Object.assign({ className: "xc-page-list-op-col", title: '操作', key: '__action__', render: this._renderColumnAction }, ListPage.defaultProps.actionColumnProps, this.props.actionColumnProps))));
            }
            return columns;
        };
        // 生成操作项列表
        this._actions = () => {
            const actions = [];
            if (this.props.editAction) {
                const action = Object.assign(Object.assign({}, this.props.editAction), { label: this.props.editAction.label || "编辑" });
                actions.push(action);
            }
            this.props.otherActions.forEach((act) => {
                const action = {
                    label: act.label,
                    enable: act.enable,
                    onClick: this._onModalClick.bind(this, act),
                };
                actions.push(action);
            });
            if (this.props.deleteAction) {
                const action = {
                    label: this.props.deleteAction.label || "删除",
                    enable: this.props.deleteAction.enable,
                    onClick: this._onDeleteClick,
                };
                actions.push(action);
            }
            return actions;
        };
        // 渲染表格"操作"列
        this._renderColumnAction = (text, record) => {
            const actions = this._actions();
            const views = [];
            actions.forEach((item, index) => {
                const enabled = item.enable ? item.enable(record, this.props.mainKey) : true;
                if (!enabled) {
                    return;
                }
                const label = this._getActionLabel(item, "未知", record, this.props.mainKey);
                views.push(React.createElement("a", { key: index * 2, onClick: (e) => {
                        item.onClick && item.onClick(record, this.props.mainKey);
                        e.stopPropagation();
                    } }, label));
                views.push(React.createElement(antd_1.Divider, { key: index * 2 + 1, type: "vertical" }));
            });
            return (React.createElement("span", null, views.slice(0, views.length - 1)));
        };
        // 获取操作标签
        this._getActionLabel = (action, defaultValue, data, mainKey) => {
            const label = action.label;
            if (typeof label === "function") {
                return label && label(data, mainKey);
            }
            else if (label) {
                return label;
            }
            else {
                return defaultValue;
            }
        };
        // 其他操作的弹框模式兼容(操作后刷新数据并保留在当前页)
        this._onModalClick = (act, data, mainKey) => {
            if (act.modalClick) {
                // 成功提示
                const success = act.modalSuccessInfo || "操作成功";
                // 错误提示
                const error = act.modalErrorInfo || "";
                // 不弹框模式
                if (!act.modalTitle && !act.modalField) {
                    this._onAction(act.modalClick, data, mainKey, this.state.pageNumber, success, error);
                }
                // 弹框模式
                else {
                    // 标题栏
                    let title = act.modalTitle || "操作提示";
                    if (act.modalField && data[act.modalField]) {
                        title = data[act.modalField];
                    }
                    // 内容
                    const content = act.modalContent || "";
                    // 实际操作
                    const action = act.modalClick;
                    antd_1.Modal.confirm({
                        title: '[' + title + ']',
                        content: React.createElement(antd_1.Alert, { message: content, type: "info" }),
                        width: 500,
                        okText: '确定',
                        okType: 'danger',
                        cancelText: '取消',
                        onOk: () => { this._onAction(action, data, mainKey, this.state.pageNumber, success, error); },
                    });
                }
            }
            else if (act.onClick) {
                // 兼容原有模式
                act.onClick(data, mainKey);
            }
        };
        // 删除操作的弹框模式兼容(删除后刷新数据并保留在当前页)
        this._onDeleteClick = (data, mainKey) => {
            if (!this.props.deleteAction) {
                return;
            }
            // 弹框模式
            if (this.props.deleteAction.modalClick) {
                // 标题栏
                let title = this.props.deleteAction.modalTitle || "删除";
                if (this.props.deleteAction.modalField && data[this.props.deleteAction.modalField]) {
                    title = data[this.props.deleteAction.modalField];
                }
                // 内容
                const content = this.props.deleteAction.modalContent || "确认要删除此记录吗？";
                // 成功提示
                const success = this.props.deleteAction.modalSuccessInfo || "删除记录成功";
                // 错误提示
                const error = this.props.deleteAction.modalErrorInfo || "删除记录失败";
                // 删除操作
                const deleteAction = this.props.deleteAction.modalClick;
                // 获取当前页
                let pn = this.state.pageNumber;
                if (this.state.data.length === 1) {
                    pn -= 1;
                }
                antd_1.Modal.confirm({
                    title: '[' + title + ']',
                    content: React.createElement(antd_1.Alert, { message: content, type: "info" }),
                    width: 500,
                    okText: '确定',
                    okType: 'danger',
                    cancelText: '取消',
                    onOk: () => { this._onAction(deleteAction, data, mainKey, pn, success, error); },
                });
            }
            // 兼容原有模式
            else if (this.props.deleteAction.onClick) {
                this.props.deleteAction.onClick(data, mainKey);
            }
        };
        this._onAction = (action, data, mainKey, pn, success, error) => {
            // 先执行操作再重新加载列表
            action(data, mainKey).then(() => {
                antd_1.message.info(success);
                this._fetchData(pn, this.state.filters, this.state.sorter);
            }).catch((err) => {
                if (error === "") {
                    error = err.message || "操作失败";
                }
                antd_1.message.error(error);
            });
        };
    }
    componentDidMount() {
        if (this.props.autoLoad) {
            this._fetchData(1, {}, {});
        }
    }
    render() {
        return (React.createElement(antd_1.Table, Object.assign({ className: "xc-page-list", size: "middle", dataSource: this.state.data, pagination: this._pagination(), loading: this.state.loading, rowKey: this.props.mainKey, onChange: this._onTableChange, onRow: this.props.onRow, rowSelection: this.props.onSelectRows ? {
                selectedRowKeys: this.state.selectedKeys,
                onChange: this._onSelectRows,
            } : undefined }, ListPage.defaultProps.tableProps, this.props.tableProps), this._renderColumns()));
    }
}
exports.default = ListPage;
ListPage.defaultProps = {
    otherActions: [],
    actionColumnProps: {},
    tableProps: {},
    autoLoad: true,
};
