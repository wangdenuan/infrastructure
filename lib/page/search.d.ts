import * as React from 'react';
import { FormComponentProps } from 'antd/lib/form/Form';
import * as Field from '../config';
interface Props {
    /**
     * 标题。
     */
    title?: React.ReactNode;
    /**
     * 初始展示还是收起。
     */
    collapsed?: boolean;
    /**
     * 附加显示。
     */
    extra?: React.ReactNode;
    /**
     * 多个操作。
     */
    buttonActions?: React.ReactNode[];
    /**
     * 按钮列的列宽，默认为24，对应换新行。
     */
    buttonSpan?: number;
    /**
     * 字段列表。
     */
    fields: Field.Base.SearchConfig[];
    /**
     * 数据。
     */
    data: {
        [key: string]: any;
    };
    /**
     * 提交数据。
     * @param values 数据值
     */
    commitData: (values: any) => Promise<void>;
    /**
     * 按条件导出数据。
     * @param values 数据值
     */
    exportData?: (values: any) => Promise<void>;
    /**
     * 返回。
     */
    goBack?: () => void;
    /**
     * 字段值发生改变时的回调方法。
     * @param name 字段名称
     * @param value 字段内容
     */
    onChangeField?: (name: string, value: any) => void;
}
interface State {
}
interface SearchFormProps extends FormComponentProps, Props {
}
declare class SearchComponent extends React.PureComponent<SearchFormProps, State> {
    static defaultProps: {
        title: string;
        collapsed: boolean;
    };
    render(): JSX.Element;
    _renderFields: () => JSX.Element[];
    _onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
    _onReset: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
    _onExport: (e: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}
declare const _default: import("antd/lib/form/interface").ConnectedComponentClass<typeof SearchComponent, Pick<SearchFormProps, "data" | "title" | "fields" | "extra" | "wrappedComponentRef" | "commitData" | "onChangeField" | "collapsed" | "buttonActions" | "buttonSpan" | "exportData" | "goBack">>;
export default _default;
