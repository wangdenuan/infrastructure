"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const React = __importStar(require("react"));
const antd_1 = require("antd");
const SearchFields = __importStar(require("../field/search"));
const { Panel } = antd_1.Collapse;
class SearchComponent extends React.PureComponent {
    constructor() {
        super(...arguments);
        this._renderFields = () => {
            return this.props.fields
                .filter(i => i.options.showInSearch)
                .map((fieldItem, index) => {
                const option = {
                    span: fieldItem.options.colSpan,
                    field: Object.assign(Object.assign({}, fieldItem), { options: Object.assign(Object.assign({}, fieldItem.options), { showInSearch: true }) }),
                    form: this.props.form,
                    onChange: this.props.onChangeField,
                };
                return (React.createElement(antd_1.Col, { span: option.span, key: index }, SearchFields.get(option)));
            });
        };
        // 提交表格
        this._onSubmit = (e) => {
            e.preventDefault();
            this.props.form.validateFields(async (err, values) => {
                if (err) {
                    return;
                }
                const newValue = Object.keys(values)
                    .reduce((prv, cur) => {
                    const v = values[cur];
                    if (cur.indexOf(SearchFields.Prefix) !== 0 && v) {
                        prv[cur] = v;
                    }
                    return prv;
                }, {});
                this.props.commitData(newValue)
                    .catch((err) => {
                    antd_1.message.error(err.message);
                });
            });
        };
        // 重置表格
        this._onReset = (e) => {
            e.preventDefault();
            this.props.form.resetFields();
        };
        this._onExport = (e) => {
            e.preventDefault();
            this.props.form.validateFields(async (err, values) => {
                if (err) {
                    return;
                }
                const newValue = Object.keys(values)
                    .reduce((prv, cur) => {
                    const v = values[cur];
                    if (cur.indexOf(SearchFields.Prefix) !== 0 && v) {
                        prv[cur] = v;
                    }
                    return prv;
                }, {});
                this.props.exportData && this.props.exportData(newValue)
                    .catch((err) => {
                    antd_1.message.error(err.message);
                });
            });
        };
    }
    render() {
        let collapsed = SearchComponent.defaultProps.collapsed;
        if (this.props.collapsed !== undefined) {
            collapsed = this.props.collapsed;
        }
        return (React.createElement(antd_1.Collapse, { accordion: true, defaultActiveKey: collapsed ? "1" : "", className: "xc-search-form", expandIconPosition: "right", expandIcon: ({ isActive }) => React.createElement(antd_1.Icon, { type: "caret-right", rotate: isActive ? 90 : 0 }) },
            React.createElement(Panel, { header: this.props.title || SearchComponent.defaultProps.title, key: "1", extra: this.props.extra },
                React.createElement(antd_1.Form, { onSubmit: this._onSubmit },
                    React.createElement(antd_1.Row, { gutter: 8 },
                        this._renderFields(),
                        React.createElement(antd_1.Col, { span: this.props.buttonSpan || 24, style: { textAlign: "right" } },
                            React.createElement(antd_1.Form.Item, null,
                                React.createElement(antd_1.Button, { className: "xc-button-search", icon: "search", type: "primary", htmlType: "submit" }, "搜索"),
                                React.createElement(antd_1.Button, { className: "xc-button-reset", icon: "redo", type: "default", onClick: this._onReset }, "清空"),
                                React.createElement("span", { className: 'xc-button-area' },
                                    this.props.exportData
                                        ? React.createElement(antd_1.Button, { className: "xc-button-reset", icon: "export", type: "default", onClick: this._onExport }, "导出")
                                        : null,
                                    this.props.buttonActions
                                        ? this.props.buttonActions.map((b) => { return b; })
                                        : null,
                                    this.props.goBack
                                        ? React.createElement(antd_1.Button, { className: "xc-button-yellow", icon: "rollback", type: "primary", onClick: this.props.goBack }, "返回")
                                        : null))))))));
    }
}
SearchComponent.defaultProps = {
    title: "搜索条件",
    collapsed: true,
};
exports.default = antd_1.Form.create()(SearchComponent);
