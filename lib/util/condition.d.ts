/** 等于 */
export declare const OpEqual = "eq";
/** 不等于 */
export declare const OpNotEqual = "ne";
/** 小于 */
export declare const OpLessThan = "lt";
/** 小于等于 */
export declare const OpLessThanEqual = "lte";
/** 大于 */
export declare const OpGreaterThan = "gt";
/** 大于等于 */
export declare const OpGreaterThanEqual = "gte";
/** 包含 */
export declare const OpIn = "in";
/** 模糊查询 */
export declare const OpLike = "lk";
/** 连接符 与 */
export declare const ConnAnd = "and";
/** 连接符 或 */
export declare const ConnOr = "or";
/** 原子级条件项 */
export interface Item {
    /** 左值，一般为列名称 */
    left: string;
    /** 右值，一般为比较数值 */
    right: any;
    /** 操作符 */
    op: string;
}
/** 复合条件项 */
export interface Complex {
    /** 复合条件项 */
    subs: Complex[];
    /** 原子级条件项 */
    items: Item[];
    /** 连接符 */
    conn: string;
}
/** 处理结果 */
export declare function process(data: any): Complex;
