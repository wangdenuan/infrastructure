"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/** 等于 */
exports.OpEqual = "eq";
/** 不等于 */
exports.OpNotEqual = "ne";
/** 小于 */
exports.OpLessThan = "lt";
/** 小于等于 */
exports.OpLessThanEqual = "lte";
/** 大于 */
exports.OpGreaterThan = "gt";
/** 大于等于 */
exports.OpGreaterThanEqual = "gte";
/** 包含 */
exports.OpIn = "in";
/** 模糊查询 */
exports.OpLike = "lk";
/** 连接符 与 */
exports.ConnAnd = "and";
/** 连接符 或 */
exports.ConnOr = "or";
/** 处理结果 */
function process(data) {
    const Prefix = "__";
    const result = {
        subs: [],
        items: [],
        conn: exports.ConnAnd,
    };
    Object.keys(data).forEach((key) => {
        if (key.startsWith(Prefix)) {
            return;
        }
        result.items.push(data[key]);
    });
    return result;
}
exports.process = process;
