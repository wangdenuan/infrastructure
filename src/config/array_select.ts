/**
 * 按number选择类型。
 */

import * as Base from './base';

export const Type = "array_select";

export interface OptionItem {
    /**
     * 默认值。
     */
    defaultValue: number;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 字段的前缀值，不为空时与数字值用"-"连接形成返回结果。
     */
    preValue: string;
    /**
     * 字段的数字值。
     */
    values: number[];
    /**
     * 显示的标签，与数字值一一对应
     */
    labels: string[];
}

export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 数据源字段标签的显示长度。
     */
    colSpan: number;
}

export const DefaultOptions: Options = {
    dataSource: [],
    colSpan: 4,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
