import { ColumnProps } from 'antd/lib/table';

/**
 * 字段基础配置。
 */
export interface Options {
    [key: string]: any;
}

export const DefaultOptions: Options = {
};

/**
 * 字段，T是配置信息。
 */
export interface Config<T extends Options = Options> {
    /**
     * 字段名。
     */
    name: string;
    /**
     * 页面上展示的文字。
     */
    label: string;
    /**
     * 字段类型。
     */
    type: string;
    /**
     * 配置信息。
     */
    options: T;
}

/**
 * 基础字段生成器。
 */
export function BaseGenerator<T>(type: string, defaultOptions: T) {
    return function (name: string, label: string, options: Partial<T & Options> = {}): Config<T & Options> {
        const opt: T & Options = { ...DefaultOptions, ...defaultOptions, ...options };
        return { name, label, type, options: opt };
    };
}

/**
 * 适当情景下的字段生成器。
 */
export function Generator<T, P>(defaultOptions: P) {
    return function (base: Config<T & Options>, options: Partial<T & P & Options> = {}): Config<T & P & Options> {
        const opt: T & P & Options = { ...defaultOptions, ...base.options, ...options };
        return { ...base, options: opt };
    };
}

/**
 * 列表字段。
 */
export interface ListOptions {
    /**
     * 是否展示。
     */
    showInList: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 自定义表格列属性。
     */
    tableProps?: ColumnProps<any>;
}

export const DefaultListOptions: ListOptions = {
    showInList: true,
    label: "",
    tableProps: undefined,
};

export type ListConfig = Config<ListOptions & Options>;

/**
 * 新建字段。
 */
export interface CreateOptions {
    /**
     * 是否展示。
     */
    showInCreate: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 是否是必填项。
     */
    required: boolean;
    /**
     * 默认值。
     */
    defaultValue: any;
}

export const DefaultCreateOptions: CreateOptions = {
    showInCreate: true,
    label: "",
    required: false,
    defaultValue: undefined,
};

export type CreateConfig = Config<CreateOptions & Options>;

/**
 * 编辑字段。
 */
export interface EditOptions {
    /**
     * 是否展示。
     */
    showInEdit: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * 是否是必填项。
     */
    required: boolean;
    /**
     * 是否是只读字段。
     */
    readonly: boolean;
}

export const DefaultEditOptions: EditOptions = {
    showInEdit: true,
    label: "",
    required: false,
    readonly: false,
};

export type EditConfig = Config<EditOptions & Options>;

/**
 * 详情字段。
 */
export interface DetailOptions {
    /**
     * 是否展示。
     */
    showInDetail: boolean;
    /**
     * 标签。
     */
    label: string;
    /**
     * Span比例。
     */
    span?: number;
}

export const DefaultDetailOptions: DetailOptions = {
    showInDetail: true,
    label: "",
};

export type DetailConfig = Config<DetailOptions & Options>;

/**
 * 搜索字段。
 */
export interface SearchOptions {
    /**
     * 是否展示。
     */
    showInSearch: boolean;
    /**
     * 栅格布局的列宽。
     */
    colSpan: number;
    /**
     * 标签。
     */
    label: string;
    /**
     * 默认值。
     */
    defaultValue: any;
}

export const DefaultSearchOptions: SearchOptions = {
    showInSearch: true,
    colSpan: 6,
    label: "",
    defaultValue: undefined,
};

export type SearchConfig = Config<SearchOptions & Options>;
