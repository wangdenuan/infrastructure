/**
 * 按比特位选择类型。
 */

import * as Base from './base';

export const Type = "bit_select";

export interface OptionItem {
    /**
     * 第几个位置(从低位计数，从0开始)。
     */
    index: number;
    /**
     * 默认值。
     */
    defaultValue: number;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 取值的列表。
     */
    values: number[];
    /**
     * 显示的标签，与values一一对应。
     */
    labels: string[];
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
}

export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 基数，为2的幂数。
     */
    base: number;
    /**
     * 数据源字段标签的显示长度(仅用于编辑模式)。
     */
    colSpan: number;
    /**
     * 是否分散数据源显示(仅用于显示模式)。
     */
    row?: boolean;
    /**
     * 数据源的显示索引(仅用于显示模式)。
     */
    itemIndex?: number;
    /**
     * 值宽度(仅用于显示模式)。
     */
    valueWidth?: number;
}

export const DefaultOptions: Options = {
    dataSource: [],
    base: 2,
    colSpan: 4,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
