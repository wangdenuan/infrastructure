/**
 * 级联选择类型。
 */

import { CascaderOptionType } from 'antd/lib/cascader';
import * as Base from './base';

export const Type = "cascade_select";

export interface Options {
    /**
     * 静态数据源。
     */
    dataSource?: CascaderOptionType[];
    /**
     * 动态数据源。
     * @param values 已选中的项。
     */
    getLevelOptions?: (values: CascaderOptionType[]) => Promise<CascaderOptionType[]>;
    /**
     * 获取值对应的展示标签。
     */
    getLabel: (value: any) => Promise<string>;
    /**
     * 自动处理字段。
     */
    fieldNames: string[];
    /**
     * 占位文本。
     */
    placeholder: string | undefined;
    /**
     * 展示时的分隔符。
     */
    separator: string;
    /**
     * 值宽度。
     */
    valueWidth?: number;
}

export const DefaultOptions: Options = {
    dataSource: undefined,
    getLevelOptions: undefined,
    getLabel: (value) => Promise.resolve("" + value),
    fieldNames: [],
    placeholder: undefined,
    separator: "-",
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
