/**
 * 按char选择类型。
 */

import * as Base from './base';

export const Type = "char_select";

export interface OptionItem {
    /**
     * 默认值。
     */
    defaultValue: string;
    /**
     * 字段标签。
     */
    fieldLabel: string;
    /**
     * 字段的基础值，自动分解为空串，小写和大写。
     */
    fieldValue: string;
    /**
     * 显示的标签，与值的空串，小写和大写一一对应，数组长度只能为3。
     */
    labels: string[];
}

export interface Options {
    /**
     * 数据源。
     */
    dataSource: OptionItem[];
    /**
     * 数据源字段标签的显示长度。
     */
    colSpan: number;
}

export const DefaultOptions: Options = {
    dataSource: [],
    colSpan: 4,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
