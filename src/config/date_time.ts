/**
 * 日期/时间类型。
 */

import { TimePickerProps } from 'antd/lib/time-picker/';
import * as Base from './base';

export const Type = "date_time";

export interface Options {
    /**
     * 是否包含日期信息，不能与hasTime同时为false。
     */
    hasDate: boolean;
    /**
     * 是否包含时间信息，不能与hasDate同时为false。
     */
    hasTime: boolean | TimePickerProps;
    /**
     * 是否使用时间戳表示数据。
     */
    useTimestamp: boolean;
    /**
     * 是否使用秒来表示数据，仅当useTimestamp=true时有意义。
     */
    useSecond: boolean;
    /**
     * 日期时间格式化的格式。
     */
    dateFormat: string | undefined;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 开始时间默认值(仅用于搜索)。
     */
    startTimeValue: number;
    /**
     * 结束时间默认值(仅用于搜索)。
     */
    endTimeValue: number;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
}

export const DefaultOptions: Options = {
    hasDate: true,
    hasTime: true,
    useTimestamp: true,
    useSecond: true,
    dateFormat: undefined,
    placeholder: undefined,
    startTimeValue: 0,
    endTimeValue: 0,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
