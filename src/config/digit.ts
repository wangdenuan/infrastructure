/**
 * 数字类型。
 */

import * as Base from './base';

export const Type = "digit";

export interface Options {
    /**
     * 最大值。
     */
    max?: number;
    /**
     * 最小值。
     */
    min?: number;
    /**
     * 整数小数步长，如果为0.1，则表示小数只能有1位。
     */
    step: number;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}

export const DefaultOptions: Options = {
    step: 1,
};

// 根据步长获取保留的小数位
export function GetDotBit(value: number) {
    // 获取小数点的位置
    const idx = String(value).indexOf(".");
    if (idx < 0) {
        return 0;
    }
    // 获取小数点后的个数
    return String(value).length - (idx + 1);
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
