/**
 * 单选或多选类型。
 */

import * as Base from './base';

export const Type = "select";

export interface OptionItem {
    /**
     * 数值。
     */
    value: string | number;
    /**
     * 标签。
     */
    label: string;
}

export interface Options {
    /**
     * 是否是多选。
     */
    multiple: boolean;
    /**
     * 数据类型，string|number。
     */
    valueType: string;
    /**
     * 多选数量限制，0无限制。
     */
    limitCount: number;
    /**
     * 静态数据源。
     */
    dataSource?: OptionItem[];
    /**
     * 动态源获取数据源。
     */
    getDataSource?: () => Promise<OptionItem[]>;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 是否启用过滤。
     */
    filtered?: boolean;
    /**
     * 未知选项标签。
     */
    unknownLabel: string;
    /**
     * 全部选项的索引，只用于搜索。
     */
    allValue: string | number;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}

export const DefaultOptions: Options = {
    multiple: false,
    valueType: "string",
    limitCount: 0,
    dataSource: undefined,
    getDataSource: undefined,
    placeholder: undefined,
    unknownLabel: "未知",
    allValue: -1,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
