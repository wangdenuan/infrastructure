/**
 * 布尔类型。
 */

import * as Base from './base';

export const Type = "switch";

export interface Options {
    /**
     * 使用radio还是switch控件。
     */
    useRadio: boolean;
    /**
     * 布尔值真值对应的标签。
     */
    trueLabel: string;
    /**
     * 布尔值假值对应的标签。
     */
    falseLabel: string;
    /**
     * 是否在呈现结果时增加圆点的显示(仅在detail中有效，为true时不显示单位)。
     */
    useDot: boolean;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
}

export const DefaultOptions: Options = {
    useRadio: true,
    trueLabel: "是",
    falseLabel: "否",
    useDot: false,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
