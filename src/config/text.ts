/**
 * 文本类型。
 */

import * as Base from './base';

export const Type = "text";

export interface Options {
    /**
     * 展示的行数，如果大于1，表示为多行文本。
     */
    numberOfLine: number;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * (文本字段) 最大长度，0不限制。
     */
    maxLength: number;
    /**
     * 正则匹配表达式。
     */
    regExp: string;
    /**
     * 单位。
     */
    unitLabel?: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 单位宽度。
     */
    unitWidth?: number;
    /**
     * 自定义渲染方法
     */
    render?: (text: string) => React.ReactNode;
}

export const DefaultOptions: Options = {
    numberOfLine: 1,
    placeholder: undefined,
    maxLength: 0,
    regExp: "",
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
