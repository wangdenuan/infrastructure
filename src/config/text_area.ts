/**
 * 文本类型。
 */

import * as Base from './base';

export const Type = "text_area";

export interface Options {
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * (文本字段) 最大长度，0不限制。
     */
    maxLength: number;
    /**
     * 值宽度。
     */
    valueWidth?: number;
    /**
     * 值高度。
     */
    valueHeight?: number;
    /**
     * 最小行数。
     */
    minRows?: number;
    /**
     * 最大行数。
     */
    maxRows?: number;
}

export const DefaultOptions: Options = {
    placeholder: undefined,
    maxLength: 0,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions>(Base.DefaultCreateOptions);

export const NewEdit = Base.Generator<Options, Base.EditOptions>(Base.DefaultEditOptions);

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
