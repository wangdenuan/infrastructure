/**
 * 树状选择类型。
 */

import { TreeNodeNormal } from 'antd/lib/tree-select/interface';
import * as Base from './base';

export const Type = "tree_select";

export interface Options {
    /**
     * 是否是多选。
     */
    multiple: boolean;
    /**
     * 数据类型，string|number。
     */
    valueType: string;
    /**
     * 多选数量限制，0无限制。
     */
    limitCount: number;
    /**
     * 数据源。
     */
    getDataSource: () => Promise<TreeNodeNormal[]>;
    /**
     * 提示文本。
     */
    placeholder: string | undefined;
    /**
     * 未知选项标签。
     */
    unknownLabel: string;
    /**
     * 描述。
     */
    description?: string;
    /**
     * 是否显示tag样式。
     */
    showTag?: boolean,
    /**
     * 值宽度。
     */
    valueWidth?: number;
}

export const DefaultOptions: Options = {
    multiple: false,
    valueType: "string",
    limitCount: 0,
    getDataSource: () => Promise.resolve([]),
    placeholder: undefined,
    unknownLabel: "未知",
};

export interface ModifyOptions {
    /**
     * 最大高度。
     */
    maxHeight: number;
    /**
     * 是否展开全部。
     */
    defaultExpandAll: boolean;
}

export const DefaultModifyOptions: ModifyOptions = {
    maxHeight: 400,
    defaultExpandAll: true,
};

export const NewBase = Base.BaseGenerator<Options>(Type, DefaultOptions);

export const NewList = Base.Generator<Options, Base.ListOptions>(Base.DefaultListOptions);

export const NewCreate = Base.Generator<Options, Base.CreateOptions & ModifyOptions>({
    ...Base.DefaultCreateOptions,
    ...DefaultModifyOptions,
});

export const NewEdit = Base.Generator<Options, Base.EditOptions & ModifyOptions>({
    ...Base.DefaultEditOptions,
    ...DefaultModifyOptions,
});

export const NewDetail = Base.Generator<Options, Base.DetailOptions>(Base.DefaultDetailOptions);

export const NewSearch = Base.Generator<Options, Base.SearchOptions>(Base.DefaultSearchOptions);
