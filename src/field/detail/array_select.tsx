import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';

type ArraySelectDetail = Field.Base.Config<Field.ArraySelect.Options & Field.Base.DetailOptions & Field.Base.Options>;

class ArraySelect extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as ArraySelectDetail;
        const data = this.props.data;
        return data[field.name] === undefined ? null : (
            <div>
                {field.options.dataSource.map(this._renderLine)}
            </div>
        );
    }

    _renderLine = (item: Field.ArraySelect.OptionItem, key: number) => {
        const field = this.props.field as ArraySelectDetail;
        const values = [...item.values];
        const data = this.props.data;
        const fieldValue = data[field.name][key] + "";
        const arr = fieldValue.split("-");
        const lastIdx = arr.length - 1;
        const value = parseInt(arr[lastIdx], 10) || 0;
        const vIndex = values.findIndex((i) => i === value);
        if (vIndex < 0) {
            return null;
        }
        return (
            <div key={key} className="xc-bit-select-view">
                {item.fieldLabel + ": " + item.labels[vIndex]}
            </div>
        );
    };
}

Storage.set(Field.ArraySelect.Type, (option) => <ArraySelect {...option} />);

export default ArraySelect;
