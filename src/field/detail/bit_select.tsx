import * as React from 'react';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type BitSelectDetail = Field.Base.Config<Field.BitSelect.Options & Field.Base.DetailOptions & Field.Base.Options>;

class BitSelect extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as BitSelectDetail;
        const data = this.props.data;
        if (data[field.name] === undefined) {
            return null;
        }
        const row = field.options.row || false;
        if (row) {
            const idx = field.options.itemIndex || 0;
            const item = field.options.dataSource[idx];
            const valueLabel = this._getValueLabel(item);
            return (
                <div className="xc-detail-content">
                    <span style={{ width: field.options.valueWidth || "auto" }}>
                        {valueLabel}
                    </span>
                    {Render.description(item.description, item.showTag)}
                </div>
            );
        }
        return (
            <div>
                {field.options.dataSource.map(this._renderLine)}
            </div>
        );
    }

    _getValueLabel = (item: Field.BitSelect.OptionItem) => {
        const field = this.props.field as BitSelectDetail;
        const data = this.props.data;
        let value = data[field.name], loc = item.index;
        while (loc > 0) {
            loc = loc - 1;
            value = Math.floor(value / field.options.base);
        }
        const finalValue = value % field.options.base;
        const vIndex = item.values.findIndex((i) => i === finalValue);
        if (vIndex < 0) {
            return null;
        }
        return item.labels[vIndex];
    };

    _renderLine = (item: Field.BitSelect.OptionItem, key: number) => {
        const valueLabel = this._getValueLabel(item);
        return valueLabel === null ? null : (
            <div key={key} className="xc-bit-select-view">
                {item.fieldLabel + ": " + valueLabel}
                {Render.description(item.description, item.showTag)}
            </div>
        );
    };
}

Storage.set(Field.BitSelect.Type, (option) => <BitSelect {...option} />);

export default BitSelect;
