import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';

type CascadeSelectDetail = Field.Base.Config<Field.CascadeSelect.Options & Field.Base.DetailOptions & Field.Base.Options>;

interface State {
    label: string;
}

class CascadeSelect extends React.PureComponent<Storage.Option, State> {
    hasCustomField: boolean;

    constructor(props: Storage.Option) {
        super(props);
        const field = props.field as CascadeSelectDetail;
        this.hasCustomField = field.options.fieldNames.length > 0;
        this.state = {
            label: "",
        };
    }

    componentDidMount(): void {
        this._loadData();
    }

    UNSAFE_componentWillReceiveProps(): void {
        this._loadData();
    }

    render() {
        return (
            <div>
                {this.state.label}
            </div>
        );
    }

    _loadData = () => {
        const field = this.props.field as CascadeSelectDetail;
        const data = this.props.data;
        let value = data[field.name];
        if (this.hasCustomField) {
            value = field.options.fieldNames.map(i => data[i]);
        }
        if (value === undefined || !field.options.getLabel) {
            this.setState({label: ""});
        } else {
            const arr: string[] = Array.isArray(value) ? value : [value];
            Promise.all(arr.filter(i => i !== undefined).map(i => field.options.getLabel(i)))
                .then((result) => {
                    this.setState({label: result.join(field.options.separator)});
                });
        }
    };
}

Storage.set(Field.CascadeSelect.Type, (option) => <CascadeSelect {...option} />);

export default CascadeSelect;
