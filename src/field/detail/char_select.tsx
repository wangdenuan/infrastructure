import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';

type CharSelectDetail = Field.Base.Config<Field.CharSelect.Options & Field.Base.DetailOptions & Field.Base.Options>;

class CharSelect extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as CharSelectDetail;
        const data = this.props.data;
        return data[field.name] === undefined ? null : (
            <div>
                {field.options.dataSource.map(this._renderLine)}
            </div>
        );
    }

    _renderLine = (item: Field.CharSelect.OptionItem, key: number) => {
        const field = this.props.field as CharSelectDetail;
        const values = ["", item.fieldValue.toLowerCase(), item.fieldValue.toUpperCase()];
        const data = this.props.data;
        const fieldValue = data[field.name] + "";
        let value = "";
        if (fieldValue.indexOf(item.fieldValue.toLowerCase()) >= 0) {
            value = item.fieldValue.toLowerCase();
        } else if (fieldValue.indexOf(item.fieldValue.toUpperCase()) >= 0) {
            value = item.fieldValue.toUpperCase();
        }
        const vIndex = values.findIndex((i) => i === value);
        if (vIndex < 0) {
            return null;
        }
        return (
            <div key={key} className="xc-bit-select-view">
                {item.fieldLabel + ": " + item.labels[vIndex]}
            </div>
        );
    };
}

Storage.set(Field.CharSelect.Type, (option) => <CharSelect {...option} />);

export default CharSelect;