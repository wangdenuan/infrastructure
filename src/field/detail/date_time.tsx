import * as React from 'react';
import moment from 'moment';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type DateTimeDetail = Field.Base.Config<Field.DateTime.Options & Field.Base.DetailOptions & Field.Base.Options>;

class DateTime extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as DateTimeDetail;
        const value = field.options.useTimestamp ? this._timestamp() : this._stringFormat();
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    }

    _timestamp = () => {
        const field = this.props.field as DateTimeDetail;
        const data = this.props.data;
        let layout = "";
        if (field.options.dateFormat) {
            layout = field.options.dateFormat;
        } else {
            const items: string[] = [];
            if (field.options.hasDate) {
                items.push("YYYY-MM-DD");
            }
            if (field.options.hasTime) {
                items.push("HH:mm:ss");
            }
            layout = items.join(" ");
        }
        let value = "";
        if (data[field.name] !== undefined && data[field.name] > 0) {
            value = moment(data[field.name] * 1000).format(layout);
        }
        return value;
    };

    _stringFormat = () => {
        const field = this.props.field as DateTimeDetail;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value = data[field.name];
        }
        return value;
    };
}

Storage.set(Field.DateTime.Type, (option) => <DateTime {...option} />);

export default DateTime;
