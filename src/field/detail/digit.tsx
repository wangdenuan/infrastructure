import * as React from 'react';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type DigitDetail = Field.Base.Config<Field.Digit.Options & Field.Base.DetailOptions & Field.Base.Options>;

class DigitView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as DigitDetail;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            const bit = Field.Digit.GetDotBit(field.options.step);
            if (bit > 0) {
                value = value + (data[field.name] * field.options.step).toFixed(bit);
            } else {
                value = value + data[field.name] * field.options.step;
            }
        }
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
                {Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    }
}

Storage.set(Field.Digit.Type, (option) => <DigitView {...option} />);

export default DigitView;
