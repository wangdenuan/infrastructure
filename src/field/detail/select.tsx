import * as React from 'react';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type SelectDetail = Field.Base.Config<Field.Select.Options & Field.Base.DetailOptions & Field.Base.Options>;

interface State {
    dataSource: Field.Select.OptionItem[] | undefined;
}

class Select extends React.PureComponent<Storage.Option, State> {
    constructor(props: Storage.Option) {
        super(props);
        const field = this.props.field as SelectDetail;
        this.state = {
            dataSource: field.options.dataSource,
        };
    }

    componentDidMount(): void {
        if (this.state.dataSource === undefined) {
            const field = this.props.field as SelectDetail;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                        this.setState({ dataSource });
                    });
            } else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }

    render() {
        if (this.state.dataSource === undefined) {
            return null;
        }
        const field = this.props.field as SelectDetail;
        const data = this.props.data;
        const valueMap: { [key: string]: string } = this.state.dataSource
            .reduce((prv: { [key: string]: string }, cur: Field.Select.OptionItem) => {
                prv[cur.value] = cur.label;
                return prv;
            }, {});
        let value = "";
        if (data[field.name] !== undefined) {
            const v = data[field.name];
            const items: Array<string | number> = Array.isArray(v) ? v : [v];
            value = items
                .map(i => valueMap[i] !== undefined ? valueMap[i] : field.options.unknownLabel)
                .join(",");
        }
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
                {Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    }
}

Storage.set(Field.Select.Type, (option) => <Select {...option} />);

export default Select;
