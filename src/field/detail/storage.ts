import * as Field from '../../config';

export interface Option {
    /**
     * 字段信息。
     */
    field: Field.Base.DetailConfig;
    /**
     * 实际数据。
     */
    data: { [key: string]: any };
}

const gMap: { [fieldType: string]: (option: Option) => React.ReactNode | null } = {};

export function get(option: Option): React.ReactNode | null {
    const fieldType = option.field.type;
    if (gMap[fieldType]) {
        return gMap[fieldType](option);
    } else {
        return null;
    }
}

export function set(fieldType: string, func?: (option: Option) => React.ReactNode | null) {
    if (func) {
        gMap[fieldType] = func;
    } else {
        delete gMap[fieldType];
    }
}
