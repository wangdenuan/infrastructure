import * as React from 'react';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type SwitchDetail = Field.Base.Config<Field.Switch.Options & Field.Base.DetailOptions & Field.Base.Options>;

class Switch extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as SwitchDetail;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value = data[field.name] ? field.options.trueLabel : field.options.falseLabel;
        }
        let b = false;
        if (data[field.name] !== undefined) {
            b = data[field.name];
        }
        const useDot = field.options.useDot;
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
                {useDot
                    ? (b
                        ? <span style={{ width: "15px", height: "15px", backgroundColor: "green", borderRadius: "50%" }} />
                        : <span style={{ width: "15px", height: "15px", backgroundColor: "red", borderRadius: "50%" }} />
                    )
                    : Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth)
                }
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    };
}

Storage.set(Field.Switch.Type, (option) => <Switch {...option} />);

export default Switch;
