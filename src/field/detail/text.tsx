import * as React from 'react';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type TextDetail = Field.Base.Config<Field.Text.Options & Field.Base.DetailOptions & Field.Base.Options>;

class TextView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as TextDetail;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value += data[field.name];
        }
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                {field.options.render === undefined
                    ? <span style={{ width: field.options.valueWidth || "auto" }}>
                        {value}
                    </span>
                    : field.options.render(value)
                }
                {Render.unit(field.options.unitLabel, field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    }
}

Storage.set(Field.Text.Type, (option) => <TextView {...option} />);

export default TextView;
