import * as React from 'react';
import * as Storage from './storage';
import * as Field from '../../config';

type TextAreaDetail = Field.Base.Config<Field.TextArea.Options & Field.Base.DetailOptions & Field.Base.Options>;

class TextView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as TextAreaDetail;
        const data = this.props.data;
        let value = "";
        if (data[field.name] !== undefined) {
            value += data[field.name];
        }
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
            </div>;
    }
}

Storage.set(Field.TextArea.Type, (option) => <TextView {...option} />);

export default TextView;
