import * as React from 'react';
import { TreeNodeNormal } from 'antd/lib/tree-select/interface';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type TreeSelectDetail = Field.Base.Config<Field.TreeSelect.Options & Field.Base.DetailOptions & Field.Base.Options>;

interface State {
    tree: TreeNodeNormal[],
}

class TreeSelectView extends React.PureComponent<Storage.Option, State> {
    state: State = {
        tree: [],
    };

    componentDidMount(): void {
        this._loadData();
    }

    render() {
        const field = this.props.field as TreeSelectDetail;
        const d = this.props.data[field.name];
        const data = Array.isArray(d) ? d : [d];
        const convert = function (i: TreeNodeNormal): TreeNodeNormal[] {
            const tmp = [i];
            (i.children || []).forEach(j => {
                tmp.push(...convert(j));
            });
            return tmp;
        };
        const items = this.state.tree.reduce((prv: TreeNodeNormal[], cur: TreeNodeNormal) => {
            return [...prv, ...convert(cur)];
        }, []);
        const value = data
            .map((item) => {
                const j = items.find(i => i.value === item);
                if (j) {
                    return j.title;
                } else {
                    return field.options.unknownLabel;
                }
            })
            .join(",");
        return value === ""
            ? <div className="xc-detail-content" />
            : <div className="xc-detail-content">
                <span style={{ width: field.options.valueWidth || "auto" }}>
                    {value}
                </span>
                {Render.description(field.options.description, field.options.showTag)}
            </div>;
    }

    _loadData = () => {
        const field = this.props.field as TreeSelectDetail;
        field.options.getDataSource()
            .then((tree) => {
                this.setState({ tree });
            })
            .catch(() => {
                this.setState({ tree: [] });
            });
    };
}

Storage.set(Field.TreeSelect.Type, (option) => <TreeSelectView {...option} />);

export default TreeSelectView;
