import * as React from 'react';
import { Radio, Form, Row, Col } from 'antd';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Field from '../../config';

type ArraySelectCreate = Field.Base.Config<Field.ArraySelect.Options & Field.Base.CreateOptions & Field.Base.Options>;

class ArraySelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void {
        this._calculateResult();
    }

    render() {
        const field = this.props.field as ArraySelectCreate;
        const label = field.options.label || field.label;
        const form = this.props.form;
        form.getFieldDecorator(field.name);
        return (
            <div>
                <Form.Item label={label}>
                    {field.options.dataSource.map(this._renderItem)}
                </Form.Item>
            </div>
        );
    }

    _renderItem = (option: Field.ArraySelect.OptionItem, idx: number) => {
        const field = this.props.field as ArraySelectCreate;
        const form = this.props.form;
        const values = [...option.values];
        return (
            <Row
                className="xc-bit-select-row"
                key={idx.toString()}
            >
                <Col span={field.options.colSpan}>
                    {option.fieldLabel}
                </Col>
                <Col span={24 - field.options.colSpan}>
                    <Form.Item>
                        {form.getFieldDecorator(this._charFieldName(option), {
                            rules: [{
                                required: field.options.required,
                                message: "请填写" + option.fieldLabel,
                            }],
                            initialValue: this._getDefaultValue(option),
                        })(
                            <Radio.Group
                                name={this._charFieldName(option)}
                                onChange={this._onChange.bind(this, option)}
                            >
                                {values.map((v, i) => (
                                    <Radio key={v} value={v}>
                                        {option.labels[i]}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        )}
                    </Form.Item>
                </Col>
            </Row>
        );
    };

    _onChange = (option: Field.ArraySelect.OptionItem, e: RadioChangeEvent) => {
        const { form } = this.props;
        const value = e.target.value;
        form.setFieldsValue({
            [this._charFieldName(option)]: value,
        });
        this._calculateResult();
    };

    _calculateResult = () => {
        const field = this.props.field as ArraySelectCreate;
        const form = this.props.form;
        // 计算最后结果
        const result = field.options.dataSource.reduce((prv, cur) => {
            let { value } = prv;
            let val = form.getFieldValue(this._charFieldName(cur)) + "";
            if (cur.preValue !== "") {
                val = cur.preValue + "-" + val;
            }
            value.push(val);
            return { value };
        }, { value: new Array<string>() });
        form.setFieldsValue({
            [field.name]: result.value,
        }, () => {
            this.props.onChange && this.props.onChange(field.name, result.value);
        });
    };

    _getDefaultValue = (option: Field.ArraySelect.OptionItem) => {
        const initialValue = this.props.initialValue;
        if (initialValue === undefined) {
            return option.defaultValue;
        } else {
            let value = option.values[0];
            // 通过preValue找到对应的item
            const itemValue = initialValue.find((i: any) => { return i.startsWith(option.preValue + "-"); });
            if (!itemValue) {
                return value;
            }
            // 切分itemValue获取单项的值
            const vals = itemValue.split("-");
            if (vals.length <= 1) {
                return value;
            }
            const idx = option.values.indexOf(parseInt(vals[1], 10) || 0);
            if (idx >= 0) {
                value = option.values[idx];
            }
            return value;
        }
    };

    _charFieldName = (option: Field.ArraySelect.OptionItem) => {
        return Storage.Prefix + this.props.field.name + option.fieldLabel;
    };
}

Storage.set(Field.ArraySelect.Type, (option) => <ArraySelect {...option} />);

export default ArraySelect;
