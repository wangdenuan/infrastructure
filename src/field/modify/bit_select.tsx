import * as React from 'react';
import { Radio, Form, Row, Col } from 'antd';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type BitSelectCreate = Field.Base.Config<Field.BitSelect.Options & Field.Base.CreateOptions & Field.Base.Options>;

// TODO 整体required
class BitSelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void {
        this._calculateResult();
    }

    render() {
        const field = this.props.field as BitSelectCreate;
        const label = field.options.label || field.label;
        const form = this.props.form;
        form.getFieldDecorator(field.name);
        return (
            <div>
                <Form.Item label={label}>
                    {field.options.dataSource.map(this._renderItem)}
                </Form.Item>
            </div>
        );
    }

    _renderItem = (option: Field.BitSelect.OptionItem, idx: number) => {
        const field = this.props.field as BitSelectCreate;
        const form = this.props.form;
        return (
            <Row
                className="xc-bit-select-row"
                key={idx.toString()}
            >
                <Col span={field.options.colSpan}>
                    {option.fieldLabel}
                </Col>
                <Col span={24 - field.options.colSpan}>
                    <Form.Item>
                        {form.getFieldDecorator(this._bitFieldName(option), {
                            rules: [{
                                required: field.options.required,
                                message: "请填写" + option.fieldLabel,
                            }],
                            initialValue: this._getDefaultValue(option),
                        })(
                            <Radio.Group
                                name={this._bitFieldName(option)}
                                onChange={this._onChange.bind(this, option)}
                            >
                                {option.values.map((v, i) => (
                                    <Radio key={v} value={v}>
                                        {option.labels[i]}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        )}
                        {Render.description(option.description, option.showTag === undefined ? true : option.showTag)}
                    </Form.Item>
                </Col>
            </Row>
        );
    };

    _onChange = (option: Field.BitSelect.OptionItem, e: RadioChangeEvent) => {
        const { form } = this.props;
        const value = e.target.value;
        form.setFieldsValue({
            [this._bitFieldName(option)]: value,
        });
        this._calculateResult();
    };

    _calculateResult = () => {
        const field = this.props.field as BitSelectCreate;
        const form = this.props.form;
        // 计算最后结果
        const items = field.options.dataSource.sort((a, b) => a.index < b.index ? -1 : 1);
        const result = items.reduce((prv, cur) => {
            let { base, index, count } = prv;
            while (index < cur.index) {
                base = base * field.options.base;
                index = index + 1;
            }
            const value = form.getFieldValue(this._bitFieldName(cur)) as number;
            count = count + base * value;
            return { base, index, count };
        }, { base: 1, index: 0, count: 0 });
        form.setFieldsValue({
            [field.name]: result.count,
        }, () => {
            this.props.onChange && this.props.onChange(field.name, result.count);
        });
    };

    _getDefaultValue = (option: Field.BitSelect.OptionItem) => {
        const field = this.props.field as BitSelectCreate;
        const initialValue = this.props.initialValue;
        if (initialValue === undefined) {
            return option.defaultValue;
        } else {
            let value = initialValue, loc = option.index;
            while (loc > 0) {
                loc = loc - 1;
                value = Math.floor(value / field.options.base);
            }
            return value % field.options.base;
        }
    };

    _bitFieldName = (option: Field.BitSelect.OptionItem) => {
        return Storage.Prefix + this.props.field.name + option.index;
    };
}

Storage.set(Field.BitSelect.Type, (option) => <BitSelect {...option} />);

export default BitSelect;
