import * as React from 'react';
import { Cascader, Form } from 'antd';
import { CascaderOptionType } from 'antd/lib/cascader';
import * as Storage from './storage';
import * as Field from '../../config';

type CascadeSelectCreate = Field.Base.Config<Field.CascadeSelect.Options & Field.Base.CreateOptions & Field.Base.Options>;

export interface State {
    options: CascaderOptionType[];
}

class CascadeSelect extends React.PureComponent<Storage.Option, State> {
    hasCustomField: boolean;

    constructor(props: Storage.Option) {
        super(props);
        const field = props.field as CascadeSelectCreate;
        this.hasCustomField = field.options.fieldNames.length > 0;
        this.state = {
            options: field.options.dataSource || [],
        };
    }

    componentDidMount(): void {
        this._loadData(this.state.options);
    }

    render() {
        const field = this.props.field as CascadeSelectCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        if (this.hasCustomField) {
            field.options.fieldNames.forEach(fieldName => {
                form.getFieldDecorator(fieldName);
            });
        }
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(this.hasCustomField ? Storage.Prefix + field.name : field.name, {
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                    initialValue: this.props.initialValue
                })(
                    <Cascader
                        options={this.state.options}
                        changeOnSelect={true}
                        placeholder={field.options.placeholder}
                        style={{ width: field.options.valueWidth || 500 }}
                        loadData={!field.options.dataSource ? this._loadData : undefined}
                        onChange={this._onChange}
                    />
                )}
            </Form.Item>
        );
    }

    _loadData = (selectedOptions: CascaderOptionType[] = []) => {
        const options = [...this.state.options];
        const optLen = selectedOptions.length;
        let targetOption: CascaderOptionType | null = optLen > 0 ? selectedOptions[optLen - 1] : null;
        if (targetOption && targetOption.children && targetOption.children.length > 0) {
            return
        }
        const field = this.props.field as CascadeSelectCreate;
        if (!field.options.getLevelOptions) {
            return;
        }
        targetOption && (targetOption.loading = true);
        field.options.getLevelOptions(selectedOptions)
            .then((result) => {
                if (!targetOption) {
                    options.push(...result);
                    return
                }
                if (result.length === 0) {
                    targetOption.isLeaf = true;
                } else {
                    targetOption.children = result;
                }
            })
            .finally(() => {
                targetOption && (targetOption.loading = false);
                this.setState({ options });
            });
    };

    _onChange = (value: string[]) => {
        const field = this.props.field as CascadeSelectCreate;
        const { form, onChange } = this.props;
        if (this.hasCustomField) {
            const values: { [key: string]: any } = {};
            field.options.fieldNames.forEach((fieldName, fieldIndex) => {
                values[fieldName] = fieldIndex < value.length ? value[fieldIndex] : undefined;
            });
            form.setFieldsValue(values, () => {
                Object.keys(values).forEach(fieldName => {
                    onChange && onChange(fieldName, values[fieldName]);
                });
            });
        } else {
            onChange && onChange(field.name, value);
        }
    };
}

Storage.set(Field.CascadeSelect.Type, (option) => <CascadeSelect {...option} />);

export default CascadeSelect;
