import * as React from 'react';
import { Radio, Form, Row, Col } from 'antd';
import { RadioChangeEvent } from "antd/lib/radio";
import * as Storage from './storage';
import * as Field from '../../config';

type CharSelectCreate = Field.Base.Config<Field.CharSelect.Options & Field.Base.CreateOptions & Field.Base.Options>;

class CharSelect extends React.PureComponent<Storage.Option> {
    componentDidMount(): void {
        this._calculateResult();
    }

    render() {
        const field = this.props.field as CharSelectCreate;
        const label = field.options.label || field.label;
        const form = this.props.form;
        form.getFieldDecorator(field.name);
        return (
            <div>
                <Form.Item label={label}>
                    {field.options.dataSource.map(this._renderItem)}
                </Form.Item>
            </div>
        );
    }

    _renderItem = (option: Field.CharSelect.OptionItem, idx: number) => {
        const field = this.props.field as CharSelectCreate;
        const form = this.props.form;
        const values = ["", option.fieldValue.toLowerCase(), option.fieldValue.toUpperCase()];
        return (
            <Row
                className="xc-bit-select-row"
                key={idx.toString()}
            >
                <Col span={field.options.colSpan}>
                    {option.fieldLabel}
                </Col>
                <Col span={24 - field.options.colSpan}>
                    <Form.Item>
                        {form.getFieldDecorator(this._charFieldName(option), {
                            rules: [{
                                required: field.options.required,
                                message: "请填写" + option.fieldLabel,
                            }],
                            initialValue: this._getDefaultValue(option),
                        })(
                            <Radio.Group
                                name={this._charFieldName(option)}
                                onChange={this._onChange.bind(this, option)}
                            >
                                {values.map((v, i) => (
                                    <Radio key={v} value={v}>
                                        {option.labels[i]}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        )}
                    </Form.Item>
                </Col>
            </Row>
        );
    };

    _onChange = (option: Field.CharSelect.OptionItem, e: RadioChangeEvent) => {
        const { form } = this.props;
        const value = e.target.value;
        form.setFieldsValue({
            [this._charFieldName(option)]: value,
        });
        this._calculateResult();
    };

    _calculateResult = () => {
        const field = this.props.field as CharSelectCreate;
        const form = this.props.form;
        // 计算最后结果
        const result = field.options.dataSource.reduce((prv, cur) => {
            let { value } = prv;
            const val = form.getFieldValue(this._charFieldName(cur)) as string;
            value = value + val;
            return { value };
        }, { value: "" });
        form.setFieldsValue({
            [field.name]: result.value,
        }, () => {
            this.props.onChange && this.props.onChange(field.name, result.value);
        });
    };

    _getDefaultValue = (option: Field.CharSelect.OptionItem) => {
        const initialValue = this.props.initialValue;
        if (initialValue === undefined) {
            return option.defaultValue;
        } else {
            let value = "";
            if (initialValue.indexOf(option.fieldValue.toLowerCase()) >= 0) {
                value = option.fieldValue.toLowerCase();
            } else if (initialValue.indexOf(option.fieldValue.toUpperCase()) >= 0) {
                value = option.fieldValue.toUpperCase();
            }
            return value;
        }
    };

    _charFieldName = (option: Field.CharSelect.OptionItem) => {
        return Storage.Prefix + this.props.field.name + option.fieldLabel;
    };
}

Storage.set(Field.CharSelect.Type, (option) => <CharSelect {...option} />);

export default CharSelect;
