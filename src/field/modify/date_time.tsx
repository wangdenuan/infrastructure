import * as React from 'react';
import { DatePicker, Form, TimePicker } from 'antd';
import moment from 'moment';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type DateTimeCreate = Field.Base.Config<Field.DateTime.Options & Field.Base.CreateOptions & Field.Base.Options>;

class DateTime extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as DateTimeCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const onlyTime = !field.options.hasDate && field.options.hasTime;
        form.getFieldDecorator(field.name, {
            initialValue: this.props.initialValue,
        });
        let initialValue: moment.Moment | undefined = undefined;
        if (this.props.initialValue) {
            if (field.options.useTimestamp) {
                initialValue = moment(this.props.initialValue * this._timestampScale());
            } else {
                initialValue = moment(this.props.initialValue, this._layout());
            }
        }
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(Storage.Prefix + field.name, {
                    initialValue: initialValue,
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }]
                })(
                    onlyTime ? (
                        <TimePicker
                            placeholder={field.options.placeholder}
                            format={field.options.dateFormat}
                            style={{ width: field.options.valueWidth || 200 }}
                            onChange={this._onChange}
                        />
                    ) : (
                        <DatePicker
                            showTime={field.options.hasTime}
                            placeholder={field.options.placeholder}
                            format={field.options.dateFormat}
                            style={{ width: field.options.valueWidth || 200 }}
                            onChange={this._onChange}
                        />
                    )
                )}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }

    _onChange = (date: moment.Moment | null, _: string) => {
        const field = this.props.field as DateTimeCreate;
        const form = this.props.form;
        if (date === null) {
            if (field.options.useTimestamp) {
                form.setFieldsValue({
                    [field.name]: 0,
                });
            } else {
                form.setFieldsValue({
                    [field.name]: "",
                });
            }
        } else {
            if (field.options.useTimestamp) {
                form.setFieldsValue({
                    [field.name]: Math.floor(date.valueOf() / this._timestampScale()),
                });
            } else {
                form.setFieldsValue({
                    [field.name]: date.format(this._layout()),
                });
            }
        }
    };

    _timestampScale = () => {
        const field = this.props.field as DateTimeCreate;
        return field.options.useSecond ? 1000 : 1;
    };

    _layout = () => {
        const field = this.props.field as DateTimeCreate;
        let layout = field.options.dateFormat;
        if (layout === undefined) {
            const items = [];
            if (field.options.hasDate) {
                items.push("YYYY-MM-DD");
            }
            if (field.options.hasTime) {
                items.push("HH:mm:ss");
            }
            layout = items.join(" ");
        }
        return layout;
    };
}

Storage.set(Field.DateTime.Type, (option) => <DateTime {...option} />);

export default DateTime;
