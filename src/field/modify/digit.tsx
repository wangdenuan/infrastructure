import * as React from 'react';
import { InputNumber, Form } from 'antd';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type DigitCreate = Field.Base.Config<Field.Digit.Options & Field.Base.CreateOptions & Field.Base.Options>;

// TODO 校验整数、小数位数。
class Digit extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as DigitCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        form.getFieldDecorator(field.name, {
            initialValue: this.props.initialValue,
        });
        let initialValue = 0;
        if (this.props.initialValue) {
            const bit = Field.Digit.GetDotBit(field.options.step);
            if (bit > 0) {
                initialValue = parseFloat((this.props.initialValue * field.options.step).toFixed(bit));
            } else {
                initialValue = this.props.initialValue * field.options.step;
            }
        }
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(Storage.Prefix + field.name, {
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                    initialValue: initialValue,
                })(
                    <InputNumber
                        max={field.options.max}
                        min={field.options.min}
                        step={field.options.step}
                        style={{ width: field.options.valueWidth || 200 }}
                        onChange={this._onChange}
                    />
                )}
                {Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }

    _onChange = (value: number | undefined) => {
        if (value === undefined) {
            return;
        }
        const field = this.props.field as DigitCreate;
        const form = this.props.form;
        const bit = Field.Digit.GetDotBit(field.options.step);
        if (bit > 0) {
            form.setFieldsValue({
                [field.name]: parseFloat((value / field.options.step).toFixed(bit)),
            });
        } else {
            form.setFieldsValue({
                [field.name]: value / field.options.step,
            });
        }
    };
}

Storage.set(Field.Digit.Type, (option) => <Digit {...option} />);

export default Digit;

