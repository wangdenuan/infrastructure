import * as React from 'react';
import { Select, Form } from 'antd';
import { SelectValue } from "antd/lib/select";
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type SelectCreate = Field.Base.Config<Field.Select.Options & Field.Base.CreateOptions & Field.Base.Options>;

interface State {
    dataSource: Field.Select.OptionItem[] | undefined;
}

// TODO limitCount支持
class SelectView extends React.PureComponent<Storage.Option, State> {
    constructor(props: Storage.Option) {
        super(props);
        const field = this.props.field as SelectCreate;
        this.state = {
            dataSource: field.options.dataSource,
        };
    }

    componentDidMount(): void {
        if (this.state.dataSource === undefined) {
            const field = this.props.field as SelectCreate;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                        this.setState({ dataSource });
                    });
            } else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }

    render() {
        const field = this.props.field as SelectCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const mul = field.options.multiple;
        return (
            <Form.Item className="xc-modify-content" label={label} hasFeedback={!mul}>
                {form.getFieldDecorator(field.name, {
                    initialValue: this.props.initialValue,
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                        type: mul ? "array" : field.options.valueType,
                    }],
                })(
                    <Select
                        mode={mul ? "multiple" : "default"}
                        placeholder={field.options.placeholder}
                        style={{ width: field.options.valueWidth || 200 }}
                        showSearch={field.options.filtered}
                        showArrow={field.options.filtered}
                        filterOption={field.options.filtered
                            ? (value: string, option: any) => {
                                return option.props.children.indexOf(value) >= 0;
                            }
                            : undefined
                        }
                        onChange={this._onChange}
                    >
                        {this.state.dataSource !== undefined ? this.state.dataSource.map(this._renderSelectOption) : undefined}
                    </Select>
                )}
                {Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }

    _renderSelectOption = (opt: Field.Select.OptionItem) => {
        return (
            <Select.Option key={opt.value} value={opt.value}>
                {opt.label}
            </Select.Option>
        );
    };

    _onChange = (value: SelectValue) => {
        const { field, onChange } = this.props;
        onChange && onChange(field.name, value);
    };
}

Storage.set(Field.Select.Type, (option) => <SelectView {...option} />);

export default SelectView;
