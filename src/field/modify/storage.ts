import { WrappedFormUtils } from 'antd/lib/form/Form';
import * as Field from '../../config';

export const Prefix = "__";

export interface Option {
    /**
     * 字段信息。
     */
    field: Field.Base.CreateConfig;
    /**
     * 表格操作。
     */
    form: WrappedFormUtils;
    /**
     * 初始值。
     */
    initialValue: any;
    /**
     * 字段改变的回调函数。
     */
    onChange?: (name: string, value: any) => void;
}

const gMap: {[fieldType: string]: (option: Option) => React.ReactNode | null} = {};

export function get(option: Option): React.ReactNode | null {
    const fieldType = option.field.type;
    if (gMap[fieldType]) {
        return gMap[fieldType](option);
    } else {
        return null;
    }
}

export function set(fieldType: string, func?: (option: Option) => React.ReactNode | null) {
    if (func) {
        gMap[fieldType] = func;
    } else {
        delete gMap[fieldType];
    }
}
