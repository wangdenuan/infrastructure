import * as React from 'react';
import { Switch, Radio, Form } from 'antd';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type SwitchCreate = Field.Base.Config<Field.Switch.Options & Field.Base.CreateOptions & Field.Base.Options>;

class SwitchView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as SwitchCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        const useRadio = field.options.useRadio;
        const initialValue = this.props.initialValue || false;
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(field.name, {
                    initialValue: initialValue,
                    valuePropName: "checked",
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                })(useRadio
                    ? <Radio.Group
                        name={field.name}
                        defaultValue={initialValue}
                        style={{ width: field.options.valueWidth || 200 }}
                        onChange={this._onRadioChange}
                    >
                        <Radio value={true}>
                            {field.options.trueLabel}
                        </Radio>
                        <Radio value={false}>
                            {field.options.falseLabel}
                        </Radio>
                    </Radio.Group>
                    : <Switch
                        defaultChecked={initialValue}
                        checkedChildren={field.options.trueLabel}
                        unCheckedChildren={field.options.falseLabel}
                        style={{ width: field.options.valueWidth || "auto" }}
                        onChange={this._onSwitchChange}
                    />
                )}
                {useRadio ? null : Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }

    _onRadioChange = (e: any) => {
        const { field, onChange } = this.props;
        onChange && onChange(field.name, e.target.value);
    };

    _onSwitchChange = (value: boolean) => {
        const { field, onChange } = this.props;
        onChange && onChange(field.name, value);
    };
}

Storage.set(Field.Switch.Type, (option) => <SwitchView {...option} />);

export default SwitchView;
