import * as React from 'react';
import { Input, Form } from 'antd';
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type TextCreate = Field.Base.Config<Field.Text.Options & Field.Base.CreateOptions & Field.Base.Options>;

// TODO numberOfLines支持
class TextView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as TextCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(field.name, {
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                        max: field.options.maxLength > 0 ? field.options.maxLength : undefined,
                    }],
                    initialValue: this.props.initialValue || "",
                })(
                    <Input
                        placeholder={field.options.placeholder}
                        style={{ width: field.options.valueWidth || 200 }}
                    />
                )}
                {Render.unit(field.options.unitLabel, field.options.showTag === undefined ? true : field.options.showTag, field.options.unitWidth)}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }
}

Storage.set(Field.Text.Type, (option) => <TextView {...option} />);

export default TextView;
