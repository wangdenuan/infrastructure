import * as React from 'react';
import { Input, Form } from 'antd';
import * as Storage from './storage';
import * as Field from '../../config';

const { TextArea } = Input;

type TextCreate = Field.Base.Config<Field.TextArea.Options & Field.Base.CreateOptions & Field.Base.Options>;

// TODO numberOfLines支持
class TextView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as TextCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(field.name, {
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                        max: field.options.maxLength > 0 ? field.options.maxLength : undefined,
                    }],
                    initialValue: this.props.initialValue || "",
                })(
                    <TextArea
                        placeholder={field.options.placeholder}
                        style={{ width: field.options.valueWidth || 200, height: field.options.valueHeight || 60 }}
                        autoSize={{ minRows: field.options.minRows || 2, maxRows: field.options.maxRows || 6 }}
                    />
                )}
            </Form.Item>
        );
    }
}

Storage.set(Field.TextArea.Type, (option) => <TextView {...option} />);

export default TextView;
