import * as React from 'react';
import { Form, TreeSelect } from 'antd';
import { TreeNodeNormal, TreeNodeValue } from "antd/lib/tree-select/interface";
import * as Storage from './storage';
import * as Render from '../util/render';
import * as Field from '../../config';

type TreeSelectCreate = Field.Base.Config<Field.TreeSelect.ModifyOptions & Field.TreeSelect.Options & Field.Base.CreateOptions & Field.Base.Options>;

export interface State {
    tree: TreeNodeNormal[];
}

class TreeSelectView extends React.PureComponent<Storage.Option, State> {
    state: State = {
        tree: [],
    };

    componentDidMount(): void {
        this._loadData();
    }

    render() {
        const field = this.props.field as TreeSelectCreate;
        const form = this.props.form;
        const label = field.options.label || field.label;
        return (
            <Form.Item className="xc-modify-content" label={label}>
                {form.getFieldDecorator(field.name, {
                    rules: [{
                        required: field.options.required,
                        message: "请填写" + label,
                    }],
                    initialValue: this.props.initialValue,
                })(
                    <TreeSelect
                        dropdownStyle={{
                            maxHeight: field.options.maxHeight,
                            overflow: 'auto',
                        }}
                        multiple={field.options.multiple}
                        treeData={this.state.tree}
                        placeholder={field.options.placeholder}
                        treeDefaultExpandAll={field.options.defaultExpandAll}
                        style={{ width: field.options.valueWidth || 500 }}
                        onChange={this._onChange}
                    />
                )}
                {Render.description(field.options.description, field.options.showTag === undefined ? true : field.options.showTag)}
            </Form.Item>
        );
    }

    _loadData = () => {
        const field = this.props.field as TreeSelectCreate;
        field.options.getDataSource()
            .then((tree) => {
                this.setState({ tree });
            })
            .catch(() => {
                this.setState({ tree: [] });
            });
    };

    _onChange = (value: TreeNodeValue) => {
        const { field, onChange } = this.props;
        onChange && onChange(field.name, value);
    };
}

Storage.set(Field.TreeSelect.Type, (option) => <TreeSelectView {...option} />);

export default TreeSelectView;
