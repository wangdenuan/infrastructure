import * as React from 'react';
import { Form, DatePicker } from 'antd';
import { RangePickerValue } from 'antd/lib/date-picker/interface';
import moment from 'moment';
import * as Storage from './storage';
import * as Field from '../../config';
import { Condition } from '../../util';

const { RangePicker } = DatePicker;

type DateTimeSearch = Field.Base.Config<Field.DateTime.Options & Field.Base.SearchOptions & Field.Base.Options>;

class DateTime extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as DateTimeSearch;
        const form = this.props.form;
        const label = field.options.label || field.label;
        form.getFieldDecorator(this._leftFieldName(), {});
        form.getFieldDecorator(this._rightFieldName(), {});
        const startTime = field.options.startTimeValue > 0 ? moment(field.options.startTimeValue * this._timestampScale()) : undefined;
        const endTime = field.options.endTimeValue > 0 ? moment(field.options.endTimeValue * this._timestampScale()) : undefined;
        return (
            <Form.Item label={label}>
                {form.getFieldDecorator(Storage.Prefix + field.name, {
                    initialValue: [startTime, endTime],
                })(
                    <RangePicker
                        onChange={this._onChange}
                        format={field.options.dateFormat}
                        showTime={field.options.hasTime}
                        style={{ width: "100%" }}
                    />
                )}
            </Form.Item>
        );
    }

    _onChange = (dates: RangePickerValue) => {
        const field = this.props.field as DateTimeSearch;
        const { form, onChange } = this.props;
        if (!dates || dates.length != 2) {
            form.setFieldsValue({
                [this._leftFieldName()]: undefined,
                [this._rightFieldName()]: undefined,
            });
            return;
        }
        if (dates[0]) {
            const c: Condition.Item = {
                left: field.name,
                right: Math.floor(dates[0].valueOf() / this._timestampScale()),
                op: Condition.OpGreaterThanEqual,
            };
            form.setFieldsValue({
                [this._leftFieldName()]: c,
            });
        }
        if (dates[1]) {
            const c: Condition.Item = {
                left: field.name,
                right: Math.floor(dates[1].valueOf() / this._timestampScale()),
                op: Condition.OpLessThan,
            };
            form.setFieldsValue({
                [this._rightFieldName()]: c,
            });
        }
        onChange && onChange(field.name, dates);
    };

    _timestampScale = () => {
        const field = this.props.field as DateTimeSearch;
        return field.options.useSecond ? 1000 : 1;
    };

    _leftFieldName = () => {
        return this.props.field.name + "_left";
    };

    _rightFieldName = () => {
        return this.props.field.name + "_right";
    };
}

Storage.set(Field.DateTime.Type, (option) => <DateTime {...option} />);

export default DateTime;
