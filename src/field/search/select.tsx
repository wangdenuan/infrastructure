import * as React from 'react';
import { Select, Form } from 'antd';
import { SelectValue } from "antd/lib/select";
import * as Storage from './storage';
import * as Field from '../../config';
import { Condition } from '../../util';

type SelectSearch = Field.Base.Config<Field.Select.Options & Field.Base.SearchOptions & Field.Base.Options>;

interface State {
    value: string | string[] | number | number[];
    dataSource: Field.Select.OptionItem[] | undefined;
}

class SelectView extends React.PureComponent<Storage.Option, State> {
    constructor(props: Storage.Option) {
        super(props);
        const field = this.props.field as SelectSearch;
        this.state = {
            value: [],
            dataSource: field.options.dataSource,
        };
    }

    componentDidMount(): void {
        if (this.state.dataSource === undefined) {
            const field = this.props.field as SelectSearch;
            if (field.options.getDataSource) {
                field.options.getDataSource()
                    .then((dataSource) => {
                        this.setState({dataSource});
                    });
            } else {
                throw new Error("数据源静态和动态必须二选一");
            }
        }
    }

    render() {
        const field = this.props.field as SelectSearch;
        const form = this.props.form;
        form.getFieldDecorator(field.name, {});
        const label = field.options.label || field.label;
        const mul = field.options.multiple;
        return (
            <Form.Item label={label}>
                {form.getFieldDecorator(Storage.Prefix + field.name, {
                    initialValue: field.options.defaultValue,
                    rules: [{
                        type: mul ? "array" : field.options.valueType,
                    }],
                })(
                    <Select
                        mode={mul ? "multiple" : "default"}
                        placeholder={field.options.placeholder}
                        onChange={this._onChange}
                    >
                        {this.state.dataSource !== undefined ? this.state.dataSource.map(this._renderSelectOption) : undefined}
                    </Select>
                )}
            </Form.Item>
        );
    }

    _renderSelectOption = (opt: Field.Select.OptionItem) => {
        return (
            <Select.Option key={opt.value} value={opt.value}>
                {opt.label}
            </Select.Option>
        );
    };

    _onChange = (value: SelectValue) => {
        const { field, form, onChange } = this.props;
        const c: Condition.Item = {
            left: field.name,
            right: value,
            op: field.options.multiple ? Condition.OpIn : Condition.OpEqual,
        };
        form.setFieldsValue({
            [field.name]: c,
        });
        onChange && onChange(field.name, value);
    };
}

Storage.set(Field.Select.Type, (option) => <SelectView {...option} />);

export default SelectView;
