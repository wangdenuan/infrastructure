import * as React from 'react';
import { Input, Form } from 'antd';
import * as Storage from './storage';
import * as Field from '../../config';
import { Condition } from '../../util';

type TextSearch = Field.Base.Config<Field.Text.Options & Field.Base.SearchOptions & Field.Base.Options>;

class TextView extends React.PureComponent<Storage.Option> {
    render() {
        const field = this.props.field as TextSearch;
        const form = this.props.form;
        form.getFieldDecorator(field.name, {});
        const label = field.options.label || field.label;
        return (
            <Form.Item label={label}>
                {form.getFieldDecorator(Storage.Prefix + field.name, {
                    rules: [{
                        max: field.options.maxLength > 0 ? field.options.maxLength : undefined,
                    }],
                    initialValue: field.options.defaultValue,
                })(
                    <Input
                        placeholder={field.options.placeholder}
                        onChange={this._onChange}
                    />
                )}
            </Form.Item>
        );
    }

    _onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        const field = this.props.field as TextSearch;
        const { form, onChange } = this.props;
        if (value === "") {
            form.setFieldsValue({
                [field.name]: undefined,
            });
        } else {
            const c: Condition.Item = {
                left: field.name,
                right: value,
                op: Condition.OpLike,
            };
            form.setFieldsValue({
                [field.name]: c,
            });
        }
        onChange && onChange(field.name, value);
    };
}

Storage.set(Field.Text.Type, (option) => <TextView {...option} />);

export default TextView;
