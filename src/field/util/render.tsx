import * as React from 'react';
import { Tag } from 'antd';

export function unit(unit?: string, tag?: boolean, witdh?: number) {
    return unit
        ? (tag === true
            ? <Tag
                color="#2db7f5"
                style={{ width: witdh || "auto", textAlign: "center" }}
            >
                {unit}
            </Tag>
            : <span>{unit}</span>
        ) : null;
}

export function description(desc?: string, tag?: boolean) {
    return desc
        ? (tag === true
            ? <Tag color="blue">{desc}</Tag>
            : <span>{unit}</span>
        ) : null;
}
