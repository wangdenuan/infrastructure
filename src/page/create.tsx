/**
 * 通用的新建页。
 */

import * as React from 'react';
import { message, Form, Button, Alert } from 'antd';
import { FormItemProps } from "antd/lib/form";
import { AlertProps } from 'antd/lib/alert/index';
import { FormComponentProps, FormProps } from 'antd/lib/form/Form';
import * as Field from '../config';
import * as DetailFields from '../field/detail';
import * as ModifyFields from '../field/modify';

interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.CreateConfig[];
    /**
     * 提交按钮的自定义标签。
     */
    submitLabel: string;
    /**
     * 提交数据。
     * @param values 数据值
     */
    commitData: (values: any) => Promise<void>;
    /**
     * 字段值发生改变时的回调方法。
     * @param name 字段名称
     * @param value 字段内容
     */
    onChangeField?: (name: string, value: any) => void;
    /**
     * 表单的配置属性。
     */
    formProps: FormProps;
    /**
     * 提交按钮的表单Item配置属性。
     */
    submitFormItemProps: FormItemProps;
    /**
     * 操作按钮。
     */
    action?: React.ReactNode[];
    /**
    * 操作说明。
    */
    actionDesc?: string;
    /**
     * 描述展示类型。
     */
    descType?: AlertProps["type"];
}

interface CreateFormProps extends FormComponentProps, Props { }

class CreateComponent extends React.PureComponent<CreateFormProps> {
    static defaultProps = {
        submitLabel: "提交",
        formProps: {},
        submitFormItemProps: {},
    };

    defaultFormProps: FormProps = {
        layout: "horizontal",
        labelCol: {
            xs: { span: 4 },
            sm: { span: 4 },
        },
        wrapperCol: {
            xs: { span: 20 },
            sm: { span: 20 },
        },
    };

    defaultSubmitFormItemProps: FormItemProps = {
        labelCol: {
            xs: { span: 4 },
            sm: { span: 4 },
        },
        wrapperCol: {
            xs: { span: 20 },
            sm: { span: 20 },
        },
    };

    render() {
        const fields = this.props.fields.filter(i => i.options.showInCreate);
        return (
            <Form
                className="xc-create-form"
                onSubmit={this._onSubmit}
                {...this.defaultFormProps}
                {...this.props.formProps}
            >
                {fields.map(this._renderField)}
                {this._renderSubmit()}
            </Form>
        );
    }

    // 渲染单个字段
    _renderField = (field: Field.Base.CreateConfig) => {
        if (field.options.readonly) {
            const option = {
                field: {
                    ...field,
                    options: {
                        ...field.options,
                        showInDetail: true,
                    }
                },
                data: field.options.defaultValue,
            };
            const label = field.options.label || field.label;
            return (
                <Form.Item
                    style={{ height: "auto" }}
                    key={field.name}
                    label={label}
                >
                    {DetailFields.get(option)}
                </Form.Item>
            );
        } else {
            const option = {
                field,
                form: this.props.form,
                initialValue: field.options.defaultValue,
                onChange: this.props.onChangeField,
            };
            const CreateFieldNode = ModifyFields.get(option);
            return (
                <div key={field.name}>
                    {CreateFieldNode}
                </div>
            );
        }
    };

    // 渲染按钮
    _renderSubmit = () => {
        return (
            <Form.Item
                className="xc-form-button"
                label={" "}
                colon={false}
                style={{ height: "auto" }}
                {...this.defaultSubmitFormItemProps}
                {...this.props.submitFormItemProps}
            >
                <Button
                    className="xc-button-default"
                    icon="check"
                    type="primary"
                    htmlType="submit"
                >
                    {this.props.submitLabel}
                </Button>
                {this.props.action && this.props.action.map((node) => {
                    return node;
                })}
                <Button
                    className="xc-button-reset"
                    icon="redo"
                    type="default"
                    onClick={this._onReset}
                >
                    {"重置"}
                </Button>
                {this.props.actionDesc
                    ? this.props.descType === undefined
                        ? this.props.actionDesc
                        : <Alert
                            type={this.props.descType}
                            message={this.props.actionDesc}
                            showIcon={true}
                        />
                    : null
                }
            </Form.Item>
        );
    };

    // 提交表格
    _onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            if (err) {
                return
            }
            const process = (item: { [key: string]: any }) => {
                return Object.keys(item).reduce((prv: { [key: string]: any }, cur: string) => {
                    const v = item[cur];
                    if (v && typeof v === 'object' && !Array.isArray(v)) {
                        prv[cur] = process(v);
                    } else if (cur.indexOf(ModifyFields.Prefix) !== 0) {
                        prv[cur] = v;
                    }
                    return prv;
                }, {});
            };
            const newValue = process(values);
            console.log(newValue);
            this.props.commitData(newValue)
                .catch((err: Error) => {
                    message.error(err.message);
                });
        });
    };

    // 重置表格
    _onReset = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        this.props.form.resetFields();
    };
}

export default Form.create<CreateFormProps>()(CreateComponent);
