/**
 * 通用的详情页。
 */

import * as React from 'react';
import { Tabs, Descriptions } from 'antd';
import { DescriptionsProps } from 'antd/lib/descriptions';
import * as Field from '../config';
import * as DetailFields from '../field/detail';

const { TabPane } = Tabs;


interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.DetailConfig[];
    /**
     * 展示的主标题。
     */
    title: string;
    /**
     * 详情数据。
     */
    data: { [key: string]: any };
    /**
     * 主标题宽度。
     */
    titleWidth?: number;
    /**
     * 字段标题宽度。
     */
    labelWidth?: number;
    /**
     * 字段内容宽度。
     */
    dataWidth?: number;
    /**
     * 详情组件的自定义属性。
     */
    descriptionProps?: DescriptionsProps;
    /**
     * 操作区域。
     */
    action?: React.ReactNode;
}

export default class extends React.PureComponent<Props> {
    render() {
        return this.props.title === ""
            ? this._renderWithoutTitle()
            : this._renderWithTitle();
    }

    _renderWithTitle = () => {
        return (
            <Tabs className="xc-tab" type="card">
                <TabPane
                    tab={(
                        <div style={{ minWidth: this.props.titleWidth || 118 }}>
                            {this.props.title}
                        </div>
                    )}
                    key="1"
                >
                    {this._renderWithoutTitle()}
                </TabPane>
            </Tabs>
        );
    };

    _renderWithoutTitle = () => {
        const fields = this.props.fields.filter(i => i.options.showInDetail);
        return (
            <div>
                <Descriptions
                    size="small"
                    layout="horizontal"
                    bordered={true}
                    {...this.props.descriptionProps}
                >
                    {fields.map(this._renderField)}
                </Descriptions>
                {this._renderAction()}
            </div>
        );
    };

    _renderField = (field: Field.Base.DetailConfig) => {
        const labelWidth = this.props.labelWidth || 118;
        let dataWidth = this.props.dataWidth || "auto";
        if (field.options.span && field.options.span > 1) {
            if (this.props.dataWidth) {
                dataWidth = this.props.dataWidth * field.options.span + labelWidth * (field.options.span - 1);
            } else {
                dataWidth = labelWidth * (2 * field.options.span - 1);
            }
        }
        if (field.type === Field.BitSelect.Type && field.options.row) {
            // BitSelect且设置row=true则返回多个Descriptions.Item
            const items: React.ReactNode[] = [];
            field.options.dataSource.forEach((item: any, idx: number) => {
                const option = {
                    field: {
                        ...field,
                        options: {
                            ...field.options,
                            itemIndex: idx,
                        },
                    },
                    data: this.props.data,
                };
                const DetailFieldNode = DetailFields.get(option);
                items.push(
                    <Descriptions.Item
                        key={idx}
                        label={(
                            <div style={{ width: labelWidth }}>
                                {item.fieldLabel}
                            </div>
                        )}
                        span={field.options.span}
                    >
                        <div style={{ width: dataWidth }}>
                            {DetailFieldNode}
                        </div>
                    </Descriptions.Item>
                );
            });
            return items;
        } else {
            // 返回单个Descriptions.Item
            const option = { field, data: this.props.data };
            const label = field.options.label || field.label;
            const DetailFieldNode = DetailFields.get(option);
            return (
                <Descriptions.Item
                    key={field.name}
                    label={(
                        <div style={{ width: labelWidth }}>
                            {label}
                        </div>
                    )}
                    span={field.options.span}
                >
                    <div style={{ width: dataWidth }}>
                        {DetailFieldNode}
                    </div>
                </Descriptions.Item>
            );
        }
    };

    // 渲染按钮
    _renderAction = () => {
        const labelWidth = this.props.labelWidth || 118;
        return this.props.action
            ? <Descriptions
                className="xc-detail-button"
                size="small"
                layout="horizontal"
                bordered={true}
            >
                <Descriptions.Item label={(<div style={{ width: labelWidth }}>{" "}</div>)}>
                    {this.props.action}
                </Descriptions.Item >
            </Descriptions>
            : null;
    };
}
