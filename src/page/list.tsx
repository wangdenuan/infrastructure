/**
 * 通用的列表页。
 */
import * as React from 'react';
import { Table, Divider, Modal, Alert, message } from 'antd';
import { PaginationConfig } from 'antd/lib/pagination';
import { TableEventListeners, TableProps, ColumnProps } from 'antd/lib/table';
import * as Field from '../config';
import * as DetailFields from '../field/detail';

export interface FetchDataResult {
    /**
     * 数据。
     */
    data: Array<{ [key: string]: any }>;
    /**
     * 总页数。
     */
    count: number;
}

export interface Action {
    /**
     * 文字内容。
     */
    label?: React.ReactNode | ((data: any, mainKey: string) => React.ReactNode);
    /**
     * 是否启用。
     */
    enable?: (data: any, mainKey: string) => boolean;
    /**
     * 点击事件。
     */
    onClick?: (data: any, mainKey: string) => Promise<void> | void;
    /**
     * 弹框模式的点击事件。
     */
    modalClick?: (data: any, mainKey: string) => Promise<void>;
    /**
    * 弹框模式的标题栏(优先modalField)。
    */
    modalTitle?: string;
    /**
    * 弹框模式标题栏的取值字段(如果modalField和modalTitle都未设置，则不是弹框)。
    */
    modalField?: string;
    /**
    * 弹框模式的内容。
    */
    modalContent?: string;
    /**
   * 弹框模式的操作成功提示。
   */
    modalSuccessInfo?: string;
    /**
   * 弹框模式的操作错误提示。
   */
    modalErrorInfo?: string;
}

interface Props {
    /**
     * 字段列表。
     */
    fields: Field.Base.ListConfig[];
    /**
     * 页大小，undefined表示不分页。
     */
    pageSize?: number;
    /**
     * 数据主键名称。
     */
    mainKey: string;
    /**
     * 分页获取列表数据。
     */
    fetchData: (pageNumber: number, pageSize: number, filters?: any, sorter?: any) => Promise<FetchDataResult>;
    /**
     * 行点击事件。
     */
    onRow?: (record: any, index: number) => TableEventListeners;
    /**
     * 选择行事件。
     */
    onSelectRows?: (selectedKeys: any[], isAll: boolean) => void;
    /**
     * 右侧编辑操作。
     */
    editAction?: Action;
    /**
     * 右侧删除操作。
     */
    deleteAction?: Action;
    /**
     * 右侧其他操作。
     */
    otherActions: Action[];
    /**
     * 操作列的附加属性。
     */
    actionColumnProps?: ColumnProps<any>;
    /**
     * 附加的属性。
     */
    tableProps?: TableProps<any>;
    /**
     * 是否自动加载。
     */
    autoLoad: boolean;
}

interface State {
    /**
     * 是否正在加载。
     */
    loading: boolean;
    /**
     * 实际数据。
     */
    data: Array<{ [key: string]: any }>;
    /**
     * 选择行。
     */
    selectedKeys: any[];
    /**
     * 当前页码。
     */
    pageNumber: number;
    /**
     * 总页码
     */
    pageCount: number;
    /**
     * 过滤条件。
     */
    filters: any;
    /**
     * 排序规则。
     */
    sorter: any;
}

export default class ListPage extends React.PureComponent<Props, State> {
    static defaultProps = {
        otherActions: [],
        actionColumnProps: {},
        tableProps: {},
        autoLoad: true,
    };

    state: State = {
        loading: false,
        data: [],
        selectedKeys: [],
        pageNumber: 1,
        pageCount: 0,
        filters: {},
        sorter: {},
    };

    componentDidMount(): void {
        if (this.props.autoLoad) {
            this._fetchData(1, {}, {});
        }
    }

    render() {
        return (
            <Table
                className="xc-page-list"
                size="middle"
                dataSource={this.state.data}
                pagination={this._pagination()}
                loading={this.state.loading}
                rowKey={this.props.mainKey}
                onChange={this._onTableChange}
                onRow={this.props.onRow}
                rowSelection={this.props.onSelectRows ? {
                    selectedRowKeys: this.state.selectedKeys,
                    onChange: this._onSelectRows,
                } : undefined}
                {...ListPage.defaultProps.tableProps}
                {...this.props.tableProps}
            >
                {this._renderColumns()}
            </Table>
        );
    }

    reload = (pageNumber?: number) => {
        const n = pageNumber === undefined ? this.state.pageNumber : pageNumber;
        this._fetchData(n, this.state.filters, this.state.sorter);
    };

    // 分页信息
    _pagination = () => {
        return this.props.pageSize === undefined ? false : {
            current: this.state.pageNumber,
            total: this.state.pageCount,
            pageSize: this.props.pageSize,
            showTotal: (total: number) => "共 " + total + " 条",
        };
    };

    // 表格切换页的回调
    _onTableChange = (pagination: PaginationConfig, filters: any, sorter: any) => {
        const pageNumber = pagination.current as number;
        this._fetchData(pageNumber, filters, sorter);
    };

    // 刷新数据
    _fetchData = (pageNumber: number, filters: any, sorter: any) => {
        this.setState({ loading: true });
        this.props.fetchData(pageNumber, this.props.pageSize || 0, Object.keys(filters).length === 0 ? undefined : filters, Object.keys(sorter).length === 0 ? undefined : sorter)
            .then((result) => {
                this.setState({
                    loading: false,
                    data: result.data,
                    pageCount: result.count,
                    pageNumber: pageNumber,
                    filters: filters,
                    sorter: sorter,
                });
            })
            .catch((err: Error) => {
                this.setState({ loading: false }, () => {
                    message.error(err.message);
                });
            });
    };

    // 选择行
    _onSelectRows = (selectedKeys: any[]) => {
        this.setState({ selectedKeys: selectedKeys }, () => {
            this.props.onSelectRows && this.props.onSelectRows(selectedKeys, this.state.data.length === selectedKeys.length);
        });
    };

    // 渲染表格列
    _renderColumns = () => {
        const columns = this.props.fields
            .filter(i => i.options.showInList)
            .map((fieldItem) => {
                return (
                    <Table.Column
                        key={fieldItem.name}
                        dataIndex={fieldItem.name}
                        title={fieldItem.options.label || fieldItem.label}
                        sorter={fieldItem.options.tableProps && fieldItem.options.tableProps.sorter ? true : false}
                        filters={fieldItem.options.filters}
                        filterMultiple={false}
                        render={(value: any) => {
                            const option = {
                                field: {
                                    ...fieldItem,
                                    options: {
                                        ...fieldItem.options,
                                        showInDetail: true,
                                    },
                                },
                                data: { [fieldItem.name]: value },
                            };
                            return DetailFields.get(option);
                        }}
                        {...fieldItem.options.tableProps}
                    />
                );
            });
        if (this._actions().length > 0) {
            columns.push((
                <Table.Column
                    className="xc-page-list-op-col"
                    title={'操作'}
                    key={'__action__'}
                    render={this._renderColumnAction}
                    {...ListPage.defaultProps.actionColumnProps}
                    {...this.props.actionColumnProps}
                />
            ));
        }
        return columns;
    };

    // 生成操作项列表
    _actions = () => {
        const actions: Action[] = [];
        if (this.props.editAction) {
            const action: Action = {
                ...this.props.editAction,
                label: this.props.editAction.label || "编辑",
            };
            actions.push(action);
        }
        this.props.otherActions.forEach((act) => {
            const action: Action = {
                label: act.label,
                enable: act.enable,
                onClick: this._onModalClick.bind(this, act),
            };
            actions.push(action);
        });
        if (this.props.deleteAction) {
            const action: Action = {
                label: this.props.deleteAction.label || "删除",
                enable: this.props.deleteAction.enable,
                onClick: this._onDeleteClick,
            };
            actions.push(action);
        }
        return actions;
    };

    // 渲染表格"操作"列
    _renderColumnAction = (text: string, record: { [key: string]: any }) => {
        const actions = this._actions();
        const views: React.ReactNode[] = [];
        actions.forEach((item, index) => {
            const enabled = item.enable ? item.enable(record, this.props.mainKey) : true;
            if (!enabled) {
                return;
            }
            const label = this._getActionLabel(item, "未知", record, this.props.mainKey);
            views.push(
                <a
                    key={index * 2}
                    onClick={(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
                        item.onClick && item.onClick(record, this.props.mainKey);
                        e.stopPropagation();
                    }}
                >
                    {label}
                </a>
            );
            views.push(<Divider key={index * 2 + 1} type="vertical" />);
        });
        return (
            <span>
                {views.slice(0, views.length - 1)}
            </span>
        );
    };

    // 获取操作标签
    _getActionLabel = (action: Action, defaultValue: string, data: any, mainKey: string) => {
        const label = action.label;
        if (typeof label === "function") {
            return label && label(data, mainKey);
        } else if (label) {
            return label;
        } else {
            return defaultValue;
        }
    };

    // 其他操作的弹框模式兼容(操作后刷新数据并保留在当前页)
    _onModalClick = (act: Action, data: any, mainKey: string) => {
        if (act.modalClick) {
            // 成功提示
            const success = act.modalSuccessInfo || "操作成功";
            // 错误提示
            const error = act.modalErrorInfo || "";
            // 不弹框模式
            if (!act.modalTitle && !act.modalField) {
                this._onAction(act.modalClick, data, mainKey, this.state.pageNumber, success, error);
            }
            // 弹框模式
            else {
                // 标题栏
                let title = act.modalTitle || "操作提示";
                if (act.modalField && data[act.modalField]) {
                    title = data[act.modalField];
                }
                // 内容
                const content = act.modalContent || "";
                // 实际操作
                const action = act.modalClick;
                Modal.confirm({
                    title: '[' + title + ']',
                    content: <Alert message={content} type="info" />,
                    width: 500,
                    okText: '确定',
                    okType: 'danger',
                    cancelText: '取消',
                    onOk: () => { this._onAction(action, data, mainKey, this.state.pageNumber, success, error); },
                });
            }
        }
        else if (act.onClick) {
            // 兼容原有模式
            act.onClick(data, mainKey);
        }
    };

    // 删除操作的弹框模式兼容(删除后刷新数据并保留在当前页)
    _onDeleteClick = (data: any, mainKey: string) => {
        if (!this.props.deleteAction) {
            return;
        }
        // 弹框模式
        if (this.props.deleteAction.modalClick) {
            // 标题栏
            let title = this.props.deleteAction.modalTitle || "删除";
            if (this.props.deleteAction.modalField && data[this.props.deleteAction.modalField]) {
                title = data[this.props.deleteAction.modalField];
            }
            // 内容
            const content = this.props.deleteAction.modalContent || "确认要删除此记录吗？";
            // 成功提示
            const success = this.props.deleteAction.modalSuccessInfo || "删除记录成功";
            // 错误提示
            const error = this.props.deleteAction.modalErrorInfo || "删除记录失败";
            // 删除操作
            const deleteAction = this.props.deleteAction.modalClick;
            // 获取当前页
            let pn = this.state.pageNumber;
            if (this.state.data.length === 1) {
                pn -= 1;
            }
            Modal.confirm({
                title: '[' + title + ']',
                content: <Alert message={content} type="info" />,
                width: 500,
                okText: '确定',
                okType: 'danger',
                cancelText: '取消',
                onOk: () => { this._onAction(deleteAction, data, mainKey, pn, success, error); },
            });
        }
        // 兼容原有模式
        else if (this.props.deleteAction.onClick) {
            this.props.deleteAction.onClick(data, mainKey);
        }
    };

    _onAction = (action: (data: any, mainKey: string) => Promise<void>, data: any, mainKey: string, pn: number, success: string, error: string) => {
        // 先执行操作再重新加载列表
        action(data, mainKey).then(() => {
            message.info(success);
            this._fetchData(pn, this.state.filters, this.state.sorter);
        }).catch((err) => {
            if (error === "") {
                error = err.message || "操作失败";
            }
            message.error(error);
        });
    };
}
