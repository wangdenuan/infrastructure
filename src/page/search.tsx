import * as React from 'react';
import { message, Form, Row, Col, Button, Collapse, Icon } from 'antd';
import { FormComponentProps } from 'antd/lib/form/Form';
import * as Field from '../config';
import * as SearchFields from '../field/search';

const { Panel } = Collapse;

interface Props {
    /**
     * 标题。
     */
    title?: React.ReactNode;
    /**
     * 初始展示还是收起。
     */
    collapsed?: boolean;
    /**
     * 附加显示。
     */
    extra?: React.ReactNode;
    /**
     * 多个操作。
     */
    buttonActions?: React.ReactNode[];
    /**
     * 按钮列的列宽，默认为24，对应换新行。
     */
    buttonSpan?: number;
    /**
     * 字段列表。
     */
    fields: Field.Base.SearchConfig[];
    /**
     * 数据。
     */
    data: { [key: string]: any };
    /**
     * 提交数据。
     * @param values 数据值
     */
    commitData: (values: any) => Promise<void>;
    /**
     * 按条件导出数据。
     * @param values 数据值
     */
    exportData?: (values: any) => Promise<void>;
    /**
     * 返回。
     */
    goBack?: () => void;
    /**
     * 字段值发生改变时的回调方法。
     * @param name 字段名称
     * @param value 字段内容
     */
    onChangeField?: (name: string, value: any) => void;
}

interface State { }

interface SearchFormProps extends FormComponentProps, Props { }

class SearchComponent extends React.PureComponent<SearchFormProps, State> {
    static defaultProps = {
        title: "搜索条件",
        collapsed: true,
    };

    render() {
        let collapsed = SearchComponent.defaultProps.collapsed;
        if (this.props.collapsed !== undefined) {
            collapsed = this.props.collapsed;
        }
        return (
            <Collapse
                accordion={true}
                defaultActiveKey={collapsed ? "1" : ""}
                className="xc-search-form"
                expandIconPosition="right"
                expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
            >
                <Panel header={this.props.title || SearchComponent.defaultProps.title} key="1" extra={this.props.extra}>
                    <Form onSubmit={this._onSubmit}>
                        <Row gutter={8}>
                            {this._renderFields()}
                            <Col span={this.props.buttonSpan || 24} style={{ textAlign: "right" }}>
                                <Form.Item>
                                    <Button
                                        className="xc-button-search"
                                        icon="search"
                                        type="primary"
                                        htmlType="submit"
                                    >
                                        {"搜索"}
                                    </Button>
                                    <Button
                                        className="xc-button-reset"
                                        icon="redo"
                                        type="default"
                                        onClick={this._onReset}
                                    >
                                        {"清空"}
                                    </Button>
                                    <span className='xc-button-area'>
                                        {this.props.exportData
                                            ? <Button
                                                className={"xc-button-reset"}
                                                icon="export"
                                                type="default"
                                                onClick={this._onExport}
                                            >
                                                {"导出"}
                                            </Button>
                                            : null
                                        }
                                        {this.props.buttonActions
                                            ? this.props.buttonActions.map((b) => { return b; })
                                            : null
                                        }
                                        {this.props.goBack
                                            ? <Button
                                                className={"xc-button-yellow"}
                                                icon={"rollback"}
                                                type={"primary"}
                                                onClick={this.props.goBack}
                                            >
                                                {"返回"}
                                            </Button>
                                            : null
                                        }
                                    </span>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Panel>
            </Collapse >
        );
    }

    _renderFields = () => {
        return this.props.fields
            .filter(i => i.options.showInSearch)
            .map((fieldItem, index) => {
                const option = {
                    span: fieldItem.options.colSpan,
                    field: {
                        ...fieldItem,
                        options: {
                            ...fieldItem.options,
                            showInSearch: true,
                        },
                    },
                    form: this.props.form,
                    onChange: this.props.onChangeField,
                };
                return (
                    <Col
                        span={option.span}
                        key={index}
                    >
                        {SearchFields.get(option)}
                    </Col>
                );
            });
    };

    // 提交表格
    _onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            if (err) {
                return
            }
            const newValue = Object.keys(values)
                .reduce((prv: { [key: string]: any }, cur: string) => {
                    const v = values[cur];
                    if (cur.indexOf(SearchFields.Prefix) !== 0 && v) {
                        prv[cur] = v;
                    }
                    return prv;
                }, {});
            this.props.commitData(newValue)
                .catch((err: Error) => {
                    message.error(err.message);
                });
        });
    };

    // 重置表格
    _onReset = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        this.props.form.resetFields();
    };

    _onExport = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            if (err) {
                return
            }
            const newValue = Object.keys(values)
                .reduce((prv: { [key: string]: any }, cur: string) => {
                    const v = values[cur];
                    if (cur.indexOf(SearchFields.Prefix) !== 0 && v) {
                        prv[cur] = v;
                    }
                    return prv;
                }, {});
            this.props.exportData && this.props.exportData(newValue)
                .catch((err: Error) => {
                    message.error(err.message);
                });
        });
    };
}

export default Form.create<SearchFormProps>()(SearchComponent);
