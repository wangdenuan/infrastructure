/** 等于 */
export const OpEqual = "eq";
/** 不等于 */
export const OpNotEqual = "ne";
/** 小于 */
export const OpLessThan = "lt";
/** 小于等于 */
export const OpLessThanEqual = "lte";
/** 大于 */
export const OpGreaterThan = "gt";
/** 大于等于 */
export const OpGreaterThanEqual = "gte";
/** 包含 */
export const OpIn = "in";
/** 模糊查询 */
export const OpLike = "lk";

/** 连接符 与 */
export const ConnAnd = "and";
/** 连接符 或 */
export const ConnOr  = "or";

/** 原子级条件项 */
export interface Item {
    /** 左值，一般为列名称 */
    left: string;
    /** 右值，一般为比较数值 */
    right: any;
    /** 操作符 */
    op: string;
}

/** 复合条件项 */
export interface Complex {
    /** 复合条件项 */
    subs: Complex[];
    /** 原子级条件项 */
    items: Item[];
    /** 连接符 */
    conn: string;
}

/** 处理结果 */
export function process(data: any): Complex {
    const Prefix = "__";
    const result: Complex = {
        subs: [],
        items: [],
        conn: ConnAnd,
    };
    Object.keys(data).forEach((key) => {
        if (key.startsWith(Prefix)) {
            return;
        }
        result.items.push(data[key]);
    });
    return result;
}
